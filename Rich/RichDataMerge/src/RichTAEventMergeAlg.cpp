/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// STL
#include <sstream>

// local
#include "RichTAEventMergeAlg.h"

using namespace Rich;

//-----------------------------------------------------------------------------
// Implementation file for class : RichTAEventMergeAlg
//
// 2008-09-23 : Christopher Rob JONES
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TAEventMergeAlg )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TAEventMergeAlg::TAEventMergeAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator )
    , m_timeTool( NULL )
    , m_mergeEvents( NULL )
    , m_eventHistorySize( 2 )
    , m_lastMainODIN( NULL )
    , m_lastSpillODIN( NULL ) {
  using namespace boost::assign;
  declareProperty( "NMergeStreams", m_nStream = 1 );
  declareProperty( "MinGPSTime", m_minGPSTime = 100 );
  {
    // Workaround for boost c++11 issue
    const std::vector<int> tmp = list_of( -4 )( -3 )( -2 )( -1 )( 1 )( 2 )( 3 )( 4 );
    declareProperty( "SpilloverLocations", m_spillLocations = tmp );
  }
}

//=============================================================================
// Destructor  de
//=============================================================================
TAEventMergeAlg::~TAEventMergeAlg() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TAEventMergeAlg::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;

  m_mergeEvents   = new MergeEvents();
  m_lastMainODIN  = new LHCb::ODIN();
  m_lastSpillODIN = new std::vector<LHCb::ODIN>();

  // Create and initialize the Event Selectors
  for ( unsigned int i = 0; i < m_nStream; ++i ) {
    std::ostringstream name;
    name << "MergeEventSelector" << i;
    IService* pISvc( 0 );
    sc = service( "EventSelector", name.str(), pISvc );
    if ( sc.isFailure() ) return sc;
    m_eventSel.push_back( dynamic_cast<IEvtSelector*>( pISvc ) );
    m_mergeIt.push_back( NULL );
    m_mergeEvents->push_back( OrderedEventData() );
    m_lastSpillODIN->push_back( LHCb::ODIN() );
  }
  m_skippedLastMainEv = false;

  info() << "Spillovers = " << m_spillLocations << endmsg;

  return sc;
}

StatusCode TAEventMergeAlg::finalize() {
  // should clean up, but causes a crash. To be understood eventually.
  // delete m_mergeEvents;
  // delete m_lastMainODIN;
  // delete m_lastSpillODIN;
  return GaudiHistoAlg::finalize();
}

void TAEventMergeAlg::printODIN( const std::string& mess, const MSG::Level level, const LHCb::ODIN* ODIN ) {
  if ( msgLevel( level ) ) {
    if ( !ODIN ) ODIN = odin();
    const Gaudi::Time eventTime = ODIN->eventTime();
    msgStream( level ) << mess << " RUN = " << ODIN->runNumber() << " Event    = " << ODIN->eventNumber()
                       << " BunchID  = " << ODIN->bunchId() << " Date " << eventTime.day( true ) << "-"
                       << eventTime.month( true ) << "-" << eventTime.year( true ) << " Time " << eventTime.hour( true )
                       << "h" << eventTime.minute( true ) << "m" << eventTime.second( true ) << "s"
                       << eventTime.nsecond() << endmsg;
  }
}

void TAEventMergeAlg::eventData( EventData& evData ) {
  evData.gpsTime          = odin()->gpsTime();
  evData.bunchId          = odin()->bunchId();
  LHCb::RawEvent* rawEv   = get<LHCb::RawEvent>( LHCb::RawEventLocation::Default );
  LHCb::RawEvent* mergeEv = &( evData.rawEvent );
  deepCopyRawEvent( rawEv, mergeEv ).ignore();
  // spillovers
  copySpills( evData.taeEvents, false );
}

void TAEventMergeAlg::copySpills( TAEvents& taeEvents, const bool mainEvent ) {
  if ( mainEvent ) m_activeSpills.clear();
  for ( std::vector<int>::const_iterator iS = m_spillLocations.begin(); iS != m_spillLocations.end(); ++iS ) {
    const std::string name = spillName( *iS );
    if ( exist<LHCb::RawEvent>( name + LHCb::RawEventLocation::Default ) ) {
      if ( mainEvent ) m_activeSpills.insert( *iS );
      if ( m_activeSpills.find( *iS ) != m_activeSpills.end() ) {
        LHCb::RawEvent* rawEv = get<LHCb::RawEvent>( name + LHCb::RawEventLocation::Default );
        delete taeEvents[name];
        taeEvents[name] = new LHCb::RawEvent();
        deepCopyRawEvent( rawEv, taeEvents[name] ).ignore();
      }
    }
  }
}

StatusCode TAEventMergeAlg::execute() {

  // clone the RawEvent from the primary event
  LHCb::RawEvent* mergedRawEvent = new LHCb::RawEvent();
  // get and copy main event(s)
  LHCb::RawEvent* rawEv = get<LHCb::RawEvent>( LHCb::RawEventLocation::Default );
  // Print ODIN for Main event
  printODIN( "Loaded Main Event ", MSG::INFO );
  auto sc = deepCopyRawEvent( rawEv, mergedRawEvent );
  if ( !sc ) return sc;

  // spillovers
  TAEvents taeEvents;
  copySpills( taeEvents, true );

  bool foundEventToMerge = false;

  // event number check
  if ( m_lastMainODIN->eventNumber() > 0 && odin()->eventNumber() < m_lastMainODIN->eventNumber() ) {
    error() << "Main event numbers going backwards ..." << endmsg;
    printODIN( " -> Last ODIN    ", MSG::ERROR, m_lastMainODIN );
    printODIN( " -> Current ODIN ", MSG::ERROR, odin() );
  }
  *m_lastMainODIN             = *odin();
  const LHCb::ODIN& main_odin = *m_lastMainODIN;

  try {
    for ( unsigned int iStream = 0; iStream < m_nStream; ++iStream ) {
      OrderedEventData& orderedData = ( *m_mergeEvents )[iStream];

      bool goOn = true;
      while ( goOn ) {
        if ( !m_skippedLastMainEv ) {
          // read new event EventData for this stream
          sc = newEvent( iStream );
          if ( sc.isFailure() ) return sc;
          // load the new event
          EventData* mergeEventData = new EventData();
          eventData( *mergeEventData );
          orderedData.push_front( mergeEventData );
          // delete data out the back
          if ( orderedData.size() > m_eventHistorySize ) {
            delete orderedData.back();
            orderedData.pop_back();
          }
        }

        // do we have enough merge event s to start comparing
        if ( orderedData.size() == m_eventHistorySize ) {

          info() << "Main  gpsTime = " << main_odin.gpsTime() << " bunchID = " << main_odin.bunchId() << endmsg;
          info() << "Merge gpsTime = " << orderedData.front()->gpsTime << " bunchID = " << orderedData.front()->bunchId
                 << endmsg;

          if ( orderedData.front()->gpsTime > main_odin.gpsTime() &&
               orderedData.back()->gpsTime > main_odin.gpsTime() ) {
            goOn              = false;
            foundEventToMerge = false;
            debug() << " -> Skipping to next main event" << endmsg;
            m_skippedLastMainEv = true;
          } else if ( orderedData.front()->gpsTime > main_odin.gpsTime() &&
                      orderedData.back()->gpsTime < main_odin.gpsTime() ) {
            m_skippedLastMainEv = false;

            // const long long maxDiff = 1000000;
            const long long gpsFrontDiff = (long long)orderedData.front()->gpsTime - (long long)main_odin.gpsTime();
            const long long gpsBackDiff  = (long long)orderedData.back()->gpsTime - (long long)main_odin.gpsTime();
            // plot1D( (double)gpsFrontDiff, "Positive gps time Diff", -maxDiff, maxDiff, 100 );
            // plot1D( (double)gpsBackDiff, "Negative gps time Diff", -maxDiff, maxDiff, 100 );

            info() << " -> Found merge events either side of main " << endmsg;
            info() << "  -> main BunchID = " << main_odin.bunchId() << " before " << orderedData.back()->bunchId << "("
                   << (int)orderedData.back()->bunchId - (int)main_odin.bunchId() << ")"
                   << " after " << orderedData.front()->bunchId << "("
                   << (int)orderedData.front()->bunchId - (int)main_odin.bunchId() << ")" << endmsg;
            info() << "  -> main gpsTime = " << main_odin.gpsTime() << " before " << orderedData.back()->gpsTime << "("
                   << gpsBackDiff << ")"
                   << " after " << orderedData.front()->gpsTime << "(" << gpsFrontDiff << ")" << endmsg;

            goOn              = false;
            foundEventToMerge = true;
            // Pick the raw event
            EventData* evtData = NULL;
            if ( (int)orderedData.back()->bunchId != (int)orderedData.front()->bunchId ) {
              evtData = ( ( abs( (int)orderedData.back()->bunchId - (int)main_odin.bunchId() ) <
                            abs( (int)orderedData.front()->bunchId - (int)main_odin.bunchId() ) )
                              ? orderedData.back()
                              : orderedData.front() );
            } else {
              evtData = ( gpsBackDiff < gpsFrontDiff ? orderedData.back() : orderedData.front() );
            }
            info() << "   -> Choosen merge event at BunchID " << evtData->bunchId << " gpsTime " << evtData->gpsTime
                   << endmsg;

            sc = deepCopyRawEvent( &( evtData->rawEvent ), mergedRawEvent, true );
            for ( TAEvents::iterator iS = taeEvents.begin(); iS != taeEvents.end(); ++iS ) {
              const std::string& name = iS->first;
              if ( evtData->taeEvents.find( name ) != evtData->taeEvents.end() ) {
                LHCb::RawEvent* rawE2 = iS->second;
                LHCb::RawEvent* rawE1 = evtData->taeEvents[name];
                sc                    = deepCopyRawEvent( rawE1, rawE2, true );
              }
            }
          } else {
            // merge events need to catch up
            goOn                = true;
            m_skippedLastMainEv = false;
          }

        } // enough merge events

      } // go on

    } // different streams

  } catch ( const GaudiException& expt ) { Warning( expt.message() ).ignore(); }

  setFilterPassed( foundEventToMerge );

  if ( foundEventToMerge ) {
    SmartIF<IDataManagerSvc> mgr( eventSvc() );
    if ( mgr ) {
      sc = mgr->setRoot( "/Event", new DataObject() );
      if ( sc.isFailure() ) return Error( "Failed to set TES root" );
    }
    put( mergedRawEvent, LHCb::RawEventLocation::Default );
    for ( TAEvents::iterator iS = taeEvents.begin(); iS != taeEvents.end(); ++iS ) {
      const std::string& name = iS->first;
      LHCb::RawEvent*    rawE = iS->second;
      put( rawE, name + LHCb::RawEventLocation::Default );
    }
    ++counter( "Created Merged Event" );
    printODIN( "Created Merged Event", MSG::INFO, odin() );
  } else {
    delete mergedRawEvent;
    for ( TAEvents::iterator iS = taeEvents.begin(); iS != taeEvents.end(); ++iS ) { delete iS->second; }
  }

  return sc;
}

void TAEventMergeAlg::purgeEvent() {
  SmartIF<IDataManagerSvc> mgr( eventSvc() );
  if ( mgr ) {
    StatusCode sc = mgr->clearStore();
    if ( !sc.isSuccess() ) { Error( "Clear of Event data store failed" ).ignore(); }
  }
}

StatusCode TAEventMergeAlg::newEvent( const unsigned int iStream ) {
  StatusCode sc = StatusCode::SUCCESS;

  debug() << "Loading new event for stream" << iStream << endmsg;

  if ( 0 == m_mergeIt[iStream] ) {
    sc = m_eventSel[iStream]->createContext( m_mergeIt[iStream] );
    if ( sc.isFailure() ) { return Error( "Cannot create context", sc ); }
  }

  purgeEvent();

  IOpaqueAddress*          pA = NULL;
  SmartIF<IDataManagerSvc> mgr( eventSvc() );
  if ( mgr ) {
    sc = m_eventSel[iStream]->next( *( m_mergeIt[iStream] ) );
    if ( !sc.isSuccess() ) { return sc; }

    // Create root address and assign address to data service
    sc = m_eventSel[iStream]->createAddress( *( m_mergeIt[iStream] ), pA );
    if ( !sc.isSuccess() ) { return sc; }

    // Set root clears the event data store first
    sc = mgr->setRoot( "/Event", pA );
    if ( !sc.isSuccess() ) { return Warning( "Error declaring event root address", sc ); }

    // Load Event root
    DataObject* pObject( 0 );
    sc = eventSvc()->retrieveObject( "/Event", pObject );
    if ( !sc.isSuccess() ) { Warning( "Unable to retrieve Event root object" ).ignore(); }

    // Print to merge event ODIN
    printODIN( "Loaded Merge Event", MSG::INFO );

    // event number check
    if ( ( *m_lastSpillODIN )[iStream].eventNumber() > 0 &&
         odin()->eventNumber() < ( *m_lastSpillODIN )[iStream].eventNumber() ) {
      error() << "Merge event numbers going backwards ..." << endmsg;
      printODIN( " -> Last ODIN    ", MSG::ERROR, &( *m_lastSpillODIN )[iStream] );
      printODIN( " -> Current ODIN ", MSG::ERROR, odin() );
    }
    ( *m_lastSpillODIN )[iStream] = *odin();
  }

  return sc;
}

StatusCode TAEventMergeAlg::deepCopyRawEvent( LHCb::RawEvent* source, LHCb::RawEvent*& result, const bool skipODIN ) {
  if ( source ) {
    for ( int i = LHCb::RawBank::L0Calo; i < LHCb::RawBank::LastType; ++i ) {
      if ( !skipODIN || i != 16 ) {
        auto banks = source->banks( LHCb::RawBank::BankType( i ) );
        if ( !banks.empty() ) {
          auto target_banks = result->banks( LHCb::RawBank::BankType( i ) );
          if ( target_banks.empty() ) {
            for ( auto j = banks.begin(); j != banks.end(); ++j ) { result->addBank( *j ); }
          } else {
            std::ostringstream mess;
            mess << "Target RawEvent already has bank of type " << i;
            Warning( mess.str() ).ignore();
          }
        }
      }
    }
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}

//=============================================================================
