!-----------------------------------------------------------------------------
! Package     : Rich/RichOnlineCalib
! Responsible : Jibo He
! Purpose     : Rich online automatic calibration
!-----------------------------------------------------------------------------

! 2016-06-08 - Chris Jones
 - Only update the version to use, in the HPD image task, when publishing is
   enabled.

! 2016-05-23 - Chris Jones
 - Adapt to the improvements in RichHPDImageSummary.

! 2016-05-19 - Chris Jones
 - CPU improvements to RichOnlineHPDImageSummary
  + Avoid duplicate loops over the decoded RICH raw data, by filling the HPD
    image and occupancy plots at the same time.
  + Use one map look up per HPD to fetch both histograms, rather than two.
 - Some C++11 improvements (use unique_ptr etc.).

! 2016-05-17 - Chris Jones
 - Update sanity checks on inactive HPDs in RichOnlineHPDImageSummary to check
   RICH1 and RICH2 seperately. Also, change the threshold to allow at most 40%
   inactive HPDs. Any more and the update is aborted, and the last good cached
   list is used.
 - Create HPD occupancy histograms on-demand if required.
 - Add a requirement on the minimum average HPD occupancy before any update
   to the inactive HPD list is performed.
 - Add a requirement on the total number of hits seen before any update to the
   inactive HPD list is performed.

! 2016-05-14 - Chris Jones
 - Add startup and shutdown messsages to DIM summary logs

! 2016-05-13 - Chris Jones
 - Move DIM summary text file to dated directories.

! 2016-05-12 - Chris Jones
 - Reduce a bit the camera messages sent via the HPD image calibration
   algorithm, as with the recent improvements (reducing the number of
   files on disk at the pit) it looks like they are sent to quickly and
   some get skipped by the server...

! 2016-05-11 - Chris Jones
 - Update refractive index and HPD image calibration tasks to create a simple
   summary log with each calibration submitted to DIM, with a timestamp.
 - Turn off lines that set the file permissions. No longer really required.

! 2016-04-27 - Chris Jones
 - Add a cache for the Inactive HPD lists.
   Only update inactive list if enough events have been processed.

! 2016-04-23 - Chris Jones
 - Don't turn off all HPDs in the Inactive HPD list condition ...

! 2015-10-07 - Chris Jones
 - Do not publish calibrations for runs with 0 stats.
 - Check to see how many times in a row a calibration is submitted based
   on cached information, and warn if it exceeds a given threshold, currently
   set to 5 times.

! 2015-09-13 - Chris Jones
 - Add support to create PDF summaries for the calibration tasks.

! 2015-06-27 - Chris Jones
 - Check to see if a given run has been processed with better statistics
   in the HPD image monitor.

! 2015-06-25 - Chris Jones
 - Do not create HPD image XML conditions when running in a RICH partition.
 - Only process RICH1 or RICH2 HPDs in the image calibration when running in
   the RICH1 or RICH2 partitions respectively.

! 2015-06-08 - jonesc
 - Removed support for monitoring the DIM states such that updaets are only
   published when in PHYSICS, as it seems this might be the cause of some
   DIM deadlocks we observed in a fill yesterday... To be investigated.

! 2015-06-06 - Chris Jones
 - Add support for fitting log-z images when the first fit gave unusual fit
   errors. Designed to try and deal with image fits for HPDs with strongly
   varying occupancies.

! 2015-06-01 - Chris Jones
 - Update Refractive index calibration task to always run the fits for each
   saveset and to send the results to Camera, even if they are not published
   to DIM. By default only the final calibration of the run is published,
   although there is now also a job option to turn on sending the first fits
   for each run as well. To be tested...

! 2015-05-30 - Chris Jones
 - Add cleaned and filtered HPD images to camera messages for HPD
   image calibrations.
 - Send a camera report with a randomly selected number of OK HPD fits
   to camera.

====================== v1r1 ================================================
! 2015-05-27 - Chris Jones
 - Implement the periodic creating of the HPD calibrations during a run,
   at a tunable rate.

! 2015-05-25 - Chris Jones
 - Include fitted function in refractive index camera reports.

! 2015-05-24 - Chris Jones
 - Add Camera reports for 'strange' HPD image fits.
 - Make sure histograms used for HPD image and occupancy calibration
   are not reset during a run.

! 2015-05-23 - Chris Jones
 - Add support for camera messaging to refractive index and HPD image
   analysis tasks.
 - Add a little more safety checks to the monitors.
 - Add missing dependency on Online/EventBuilderc

! 2015-05-20 - Jibo He
 - src/RichOnlineHPDImageSummary
   . Addded histograms to get average occupancy
   . Changed PublishSvc to "true", otherwise initialize() fails

! 2015-05-18 - Jibo He
 - Changed the way how to get image hists
 - Fixed occupancy xml write out problem
 - Added precison control for HPD image analysis

! 2015-05-17 - Jibo He
 - Added calibration part for HPD Image analysis

! 2015-05-14 - Jibo He
 - HPD image calib split

! 2015-05-14 - Jibo He
 - Modified to record versions of xml of each run

! 2015-05-05 - Jibo He
 - Added function to publish to DIM
 - Adapted to new convention on names

! 2015-04-09 - Chris Jones
 - Add a new implementation of the HPD Image analysis algorithm.

! 2015-03-05 - Marco Clemencic
 - Added CMake configuration.

! 2014-09-29 - Jibo He
 - Fixed HPD id problem

! 2014-05-04 - Jibo He
 - Added codes to write out HPD image shift and occupancy params to xml file

! 2014-05-02 - Jibo He
 - Refractive index calibration part mostly done
