/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "Event/ODIN.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// Publish to DIM
#include "GaudiKernel/IPublishSvc.h"
#include "GaudiKernel/ServiceLocatorHelper.h"

// Dim Objects
#include "RichMonitoringTools/RichDimClasses.h"

// local
#include "rootstyle.h"

// CK resolution fitter
#include "RichRecUtils/RichCKResolutionFitter.h"

// ROOT
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1.h>
#include <TLine.h>
#include <TMath.h>
#include <TPad.h>
#include <TSystem.h>
#include <TText.h>

// Boost
#include "boost/algorithm/string.hpp"
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

// STD
#include <algorithm>
#include <array>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace Rich::Future::Rec::Calib {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    class ScaleFactor {
    public:
      double value{1.0};
      double error{0.0};

    public:
      friend inline std::ostream& operator<<( std::ostream& os, const ScaleFactor& sf ) {
        os << "[ " << boost::format( "%9.4e" ) % sf.value;
        if ( sf.error > 0 ) { os << " +- " << boost::format( "%7.2e" ) % sf.error; }
        return os << " ]";
      }
    };
    using ScaleFactorsCache = DetectorArray<ScaleFactor>;
  } // namespace

  /** @class RefIndexCalib RichRefIndexCalib.h
   *
   *  Runs the RICH refractive index calibration directly from photon objects
   *
   *  @author Chris Jones
   *  @date   2021-02-15
   */

  class RefIndexCalib final : public Consumer<void( const LHCb::ODIN&,                         //
                                                    const LHCb::Track::Selection&,             //
                                                    const Summary::Track::Vector&,             //
                                                    const Relations::PhotonToParents::Vector&, //
                                                    const LHCb::RichTrackSegment::Vector&,     //
                                                    const CherenkovAngles::Vector&,            //
                                                    const SIMDCherenkovPhoton::Vector&,        //
                                                    const ScaleFactorsCache& ),
                                              LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase, ScaleFactorsCache>> {

  public:
    /// Standard constructor
    RefIndexCalib( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"ScaleFactorsCache", DeRichLocations::derivedCondition( name + "-DataCache" )}} ) {

      declareProperty( "nPolFull", m_ckFitter.params().RichNPol );
      declareProperty( "StartNPol", m_ckFitter.params().RichStartPol );
      declareProperty( "MaxShiftError", m_ckFitter.params().MaxShiftError );
      declareProperty( "MaxSigmaError", m_ckFitter.params().MaxSigmaError );
      declareProperty( "RichFitMin", m_ckFitter.params().RichFitMin );
      declareProperty( "RichFitMax", m_ckFitter.params().RichFitMax );
      declareProperty( "RichFitTypes", m_ckFitter.params().RichFitTypes );

      setProperty( "NBins1DHistos", 150 ).ignore();
      setProperty( "NBins2DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN&                         odin,          //
                     const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons,       //
                     const ScaleFactorsCache&                  scaleFactors ) const override;

    /// Initialization
    StatusCode initialize() override {

      // for debug for testing
      // setProperty( "OutputLevel", MSG::DEBUG ).ignore();

      return Consumer::initialize().andThen( [&] {
        // Can we write to the configured paths ?
        // note as can change path for logs, needs to be first.
        const auto curPath = boost::filesystem::current_path().string();
        if ( !createDir( m_summaryPath.value(), false ) ) {
          const auto old_path = m_summaryPath.value();
          m_summaryPath       = curPath + "/summaries";
          calib_message( MSG::WARNING, "Cannot write to '", old_path, "' -> Resetting to '", m_summaryPath.value(),
                         "/summaries'" );
        }

        divider();
        calib_message( MSG::INFO, "Initializing" );

        // Partition
        const char* partition = getenv( "PARTITION" );
        if ( partition ) {
          const std::string part{partition};
          calib_message( MSG::INFO, "Environment variable PARTITION = '", part, "'" );
          m_okToPublish =
              m_okToPublish.value() && ( part != "RICH" && part != "RICH1" && part != "RICH2" && part != "FEST" );
        } else {
          calib_message( MSG::WARNING, "Environment variable PARTITION is not set" );
        }
        if ( !m_okToPublish ) {
          calib_message( MSG::WARNING, "Calibration writing and publishing is DISABLED" );
        } else {
          calib_message( MSG::INFO, "Calibration writing and publishing is ENABLED" );
        }

        // Derived conditions
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        addConditionDerivation(
            {Detector::Rich1::DefaultConditionKey,      //
             Detector::Rich2::DefaultConditionKey},     //
            inputLocation<ScaleFactorsCache>(),         //
            [parent = this]( const Detector::Rich1& r1, //
                             const Detector::Rich2& r2 ) {
              const ScaleFactorsCache scaleFs{ScaleFactor{r1.radiator().currentRefIndexScaleFactor(), 0.0},
                                              ScaleFactor{r2.radiator().currentRefIndexScaleFactor(), 0.0}};
              parent->calib_message( MSG::DEBUG, "Current Scalefactors : R1Gas=", scaleFs[Rich::Rich1],
                                     " R2Gas=", scaleFs[Rich::Rich2] );
              return scaleFs;
            } );
        if ( m_enableRunByRunHists ) { calib_message( MSG::INFO, "Run-by-run histograms are enabled" ); }

        // ROOT style for PDFs
        if ( m_createPDFsummary ) { setStyle(); }

        calib_message( MSG::INFO, "Writing summary data to ", m_summaryPath.value() );
        if ( !createDir( m_conditionsDbPath.value(), false ) ) {
          calib_message( MSG::WARNING, "Cannot write to '", m_conditionsDbPath.value(), "' -> Resetting to '", curPath,
                         "/conditions'" );
          m_conditionsDbPath = curPath + "/conditions";
        }
        calib_message( MSG::INFO, "Writing conditions data to ", m_conditionsDbPath.value() );

        calib_message( MSG::VERBOSE, "Task output level is ", msgLevel() );

        // Fix file permissions ...
        // fixFilePermissions();

        // Publish service
        if ( !m_okToPublish ) { m_disableDIMpublish = true; }
        if ( !m_disableDIMpublish ) {
          // Is the DIM DNS node defined ?
          const char* dimDnsNode = getenv( "DIM_DNS_NODE" );
          if ( !dimDnsNode ) {
            calib_message( MSG::WARNING, "DIM_DNS_NODE not defined. Disabling DIM publishing" );
            m_disableDIMpublish = true;
          } else {
            calib_message( MSG::INFO, "Using DIM_DNS_NODE '", dimDnsNode, "'" );
            // Initialise scale factors to last known values
            m_Rich1RefIndex = getLastRefIndexSF( "Rich1Gas" ).first;
            m_Rich2RefIndex = getLastRefIndexSF( "Rich2Gas" ).first;
            // Initialise the publish service
            auto sc = serviceLocator()->service( "LHCb::PublishSvc", m_pPublishSvc, true );
            if ( sc.isSuccess() && m_pPublishSvc ) {
              m_pPublishSvc->declarePubItem( m_Rich1TaskName.value(), m_Rich1PubString );
              m_pPublishSvc->declarePubItem( m_Rich2TaskName.value(), m_Rich2PubString );
              m_pPublishSvc->declarePubItem( m_Rich1ScaleTaskName.value(), m_Rich1RefIndex );
              m_pPublishSvc->declarePubItem( m_Rich2ScaleTaskName.value(), m_Rich2RefIndex );
              calib_message( MSG::INFO, "LHCb::PublishSvc successfully initialized" );
            } else {
              calib_message( MSG::ERROR, "Failed to instanciate LHCb::PublishSvc" );
            }
          }
        }

        resetCalibCounters();

        // write to summary file
        writeToDimSummaryFile( "Initialized" );
        calib_message( MSG::INFO, "Initialized" );
      } );
    }

    /// Print run summaries
    void runSummaries( const MSG::Level level = MSG::INFO ) const {
      if ( msgLevel( level ) && !m_processedRuns.empty() ) {
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
        calib_message( level, "|    Run    | Used Events | Late Events |  Rich1 Ver |  Rich2 Ver |" );
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
        const auto& R1Vers = m_runCalibVersions["Rich1Gas"];
        const auto& R2Vers = m_runCalibVersions["Rich2Gas"];
        for ( const auto& [run, stats] : m_processedRuns ) {
          const auto iR1V = R1Vers.find( run );
          const auto iR2V = R2Vers.find( run );
          const auto R1V  = ( iR1V != R1Vers.end() ? str( boost::format( "%8u" ) % iR1V->second ) : "     N/A" );
          const auto R2V  = ( iR2V != R2Vers.end() ? str( boost::format( "%8u" ) % iR2V->second ) : "     N/A" );
          calib_message( level, boost::format( "| %8u  |  %9u  |  %9u  | %s   | %s   |" ) //
                                    % run % stats.used % stats.late % R1V % R2V );
        }
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
      }
    }

    /// Start
    StatusCode start() override {
      divider();
      writeToDimSummaryFile( "Starting" );
      calib_message( MSG::INFO, "Starting" );
      return Consumer::start();
    }

    /// Stop
    StatusCode stop() override {
      divider();
      calib_message( MSG::INFO, "Stopping" );
      // Do not run calibration in stop() during QMT tests. Defer final one
      // to finalize() which is then included in the test ref comparisons.
      if ( m_allowStopCalib ) {
        // use periodic here incase task starts up again with the same run
        runCalibration( "Stop" );
      }
      writeToDimSummaryFile( "Stopped" );
      calib_message( MSG::INFO, "Stopped" );
      return Consumer::stop();
    }

    /// Finalize
    StatusCode finalize() override {
      divider();
      calib_message( MSG::INFO, "Finalizing" );
      // In principle calibration not needed here, as I do not think its
      // possible to be here without having stop() called first, but just
      // to be sure try again. At minimum it will trigger the resets.
      // Also needed for when running in a QMT test as there stop() calibrations are not run.
      runCalibration();
      runSummaries();
      // cleanup
      deleteCanvas();
      // write to summary file
      writeToDimSummaryFile( "Finalized" );
      calib_message( MSG::INFO, "Finalized" );
      return Consumer::finalize();
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // Loop over radiators
      for ( const auto rad : activeRadiators() ) {
        // book histogram for calibration
        h_ckResAll[rad] =
            Gaudi::Utils::Aida2ROOT::aida2root( richHisto1D( HID( "ckResAll", rad ),                           //
                                                             "Rec-Exp CKTheta - All photons",                  //
                                                             -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                                             "delta(Cherenkov theta) / rad", "Entries" ) );
        ok &= ( h_ckResAll[rad] != nullptr );
      } // active rad loop
      return StatusCode{ok};
    }

  private:
    /// Test if a fit retry should be tried for given radiator
    inline bool canRetryFit( const Rich::RadiatorType rad ) const {
      return ( m_failedFitCount[rad] > 0 && m_failedFitCount[rad] < m_maxfailedFitRetries );
    }

    /// Test if a fit retry should be tried for either radiator
    inline bool canRetryFit() const { return ( canRetryFit( Rich::Rich1Gas ) || canRetryFit( Rich::Rich2Gas ) ); }

    /// Reset the failed fit counts
    void resetFailedfitCounts( const Rich::RadiatorType rad ) const { m_failedFitCount[rad] = 0; }

    /// Reset histograms following running a calibration
    void resetHistograms() const {
      calib_message( MSG::VERBOSE, "Resetting histograms" );
      // reset run event count
      m_nEventsThisRun = 0;
      // reset histograms
      for ( const auto rad : activeRadiators() ) {
        if ( h_ckResAll[rad] ) { h_ckResAll[rad]->Reset(); }
        // reset failed fit counts
        resetFailedfitCounts( rad );
      }
    }

    /// End of calibration reset
    void resetCalibCounters() const {
      calib_message( MSG::VERBOSE, "Resetting counters" );
      m_nEventsSinceCalib = 0;               // reset event count for next time
      m_runNumber         = 0;               // correctly determine the next run number
      m_timeLastCalib     = time( nullptr ); // reset the last calibration time to now
    }

    /// Run the n-1 calibration ...
    void runCalibration( const std::string& type = "Final" ) const;

  private:
    // properties

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Overall Min monentum cut
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this,
        "MinP",
        {0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV},
        "Minimum momentum (GeV/c)"};

    /// Overall Max monentum cut
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this,
        "MaxP",
        {9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV},
        "Maximum momentum (GeV/c)"};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.0250f, 0.0055f, 0.0035f}};

    /// Minimum number of events for a calibration
    Gaudi::Property<unsigned long long> m_minEventsForCalib{this, "MinEventsForCalib", 100};

    /// Time interval to send periodic calibration updates
    Gaudi::Property<int> m_minTimeBetweenCalibs{this, "MinTimeBetweenCalibs", 16 * 60}; // 16 mins

    /// Time interval for failed fit retries
    Gaudi::Property<int> m_failedFitRetryPeriod{this, "FailedFitRetryPeriod", 20}; // 20 secs

    /// Max number of failed fit retries
    Gaudi::Property<unsigned int> m_maxfailedFitRetries{this, "MaxFailedFitRetries", 10};

    /// Detailed run-by-run debugging histos
    Gaudi::Property<bool> m_enableRunByRunHists{this, "RunByRunHists", false};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.040f, 0.020f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.065f, 0.035f}};

    /// RICH1 task name
    Gaudi::Property<std::string> m_Rich1TaskName{this, "Rich1TaskName", "Rich1/Environment/Gas"};

    /// RICH2 task name
    Gaudi::Property<std::string> m_Rich2TaskName{this, "Rich2TaskName", "Rich2/Environment/Gas"};

    /// RICH1 name for n-1 scale factor DIM service
    Gaudi::Property<std::string> m_Rich1ScaleTaskName{this, "Rich1ScaleTaskName", "Rich1RefIndex"};

    /// RICH2 name for n-1 scale factor DIM service
    Gaudi::Property<std::string> m_Rich2ScaleTaskName{this, "Rich2ScaleTaskName", "Rich2RefIndex"};

    /// Flag to turn on the creation of a Preliminary calibration for each run
    Gaudi::Property<bool> m_createPrelimCalib{this, "CreatePreliminaryCalibration", true};

    /// The ROOT file directory to use for the calibration (the Gaudi Monitor)
    Gaudi::Property<std::string> m_HistBase{this, "HistBase", "RICH/RiCKResLongTight/"};

    /// The histogram ID to use
    Gaudi::Property<std::string> m_ResPlot{this, "ResPlot", "ckResAll"};

    /// The radiators to calibration
    Gaudi::Property<std::vector<std::string>> m_rads{this, "RichRads", {"Rich1Gas", "Rich2Gas"}};

    /// Min entries for a fit
    Gaudi::Property<unsigned int> m_minEntries{this, "minEntries", 100000};

    /// CK Resolution fitter
    Rich::Rec::CKResolutionFitter m_ckFitter{this};

    /// slope from the shift
    Gaudi::Property<std::array<double, 2>> m_RefIndexSlope{this, "RefIndexSlope", {38.1, 65.25}};

    /// Precision for YML file parameters
    Gaudi::Property<unsigned int> m_Precision{this, "Precision", 8};

    /// Summary file path
    Gaudi::Property<std::string> m_summaryPath{this, "summaryPath", "/group/online/alignment"};

    /// LHCB DB to write to path
    Gaudi::Property<std::string> m_conditionsDbPath{this, "conditionsDbPath", "/group/online/hlt/conditions.run3"};

    /// Create direct run conditions
    Gaudi::Property<bool> m_makeDBRunFile{this, "MakeDBRunFile", false};

    /// log file to list all previous YML versions
    Gaudi::Property<std::string> m_ymlVersionLog{this, "ymlVersionLog", "version.txt"};

    /// log file for previous scale factors
    Gaudi::Property<std::string> m_RefIndexSFLog{this, "RefIndexSFLog", "refIndexSF.txt"};

    // Log file from run by run summary
    Gaudi::Property<std::string> m_runByrunLog{this, "RunByRunSummary", "RunByRunSummary.txt"};

    /// DIM Summary file name
    Gaudi::Property<std::string> m_dimSummaryFile{this, "DIMSummaryFile", "DIMUpdateSummary.txt"};

    /// DIM Summary file name
    Gaudi::Property<std::string> m_loggerFile{this, "LoggerFile", "Task.log"};

    /// Flag to turn on/off publishing to DIM
    Gaudi::Property<bool> m_disableDIMpublish{this, "DisableDIMPublish", false};

    /// Flag to turn on/off the saving of summary PDF files
    Gaudi::Property<bool> m_createPDFsummary{this, "CreatePDFSummary", true};

    /// maximum cached calibrations to allow in a row
    Gaudi::Property<unsigned int> m_maxCachedCalibs{this, "MaxCachedCalibsInARow", 5};

    /// Max run number history
    Gaudi::Property<unsigned int> m_runNumHistSize{this, "RunNumHistSize", 100};

    /// Is it is OK to publish calibrations
    Gaudi::Property<bool> m_okToPublish{this, "OKToPublish", true};

    /// Use a running average for the scale factors ?
    Gaudi::Property<bool> m_useRunningAv{this, "UseRunningAverage", true};

    /// Allow calibrations during stop()
    Gaudi::Property<bool> m_allowStopCalib{this, "AllowCalibAtStop", true};

  private:
    // cached data

    /// Lock for main event processing
    mutable std::mutex m_mutex;

    // CK theta histograms for calibration process
    RadiatorArray<TH1D*> h_ckResAll = {{}};

    // Current active run-by-run histograms
    mutable RadiatorArray<AIDA::IHistogram1D*>                h_ckThetaRec_run           = {{}};
    mutable RadiatorArray<AIDA::IHistogram1D*>                h_ckThetaExp_run           = {{}};
    mutable RadiatorArray<AIDA::IHistogram1D*>                h_ckThetaRes_run           = {{}};
    mutable RadiatorArray<AIDA::IHistogram1D*>                h_tkPtot_run               = {{}};
    mutable RadiatorArray<std::array<AIDA::IHistogram1D*, 4>> h_ckThetaRes_quadrants_run = {{{}}};
    mutable RadiatorArray<AIDA::IHistogram2D*>                h_tkEntryXY_run            = {{}};

    /// Run number for last processed run
    mutable unsigned int m_runNumber{0};

    /// Total overall number of events seen
    mutable unsigned long long m_nEvts{0};

    /// Number of events seen since last calibration was run...
    mutable unsigned long long m_nEventsSinceCalib{0};

    /// Total number of events in current run
    mutable unsigned long long m_nEventsThisRun{0};

    struct RunStats {
      unsigned long long used{0}; ///< #Events used for calibration
      unsigned long long late{0}; ///< #Events that arrived late
    };

    /** List of all previously calibrated runs for the current process
     *  with the number of events used for that calibration */
    mutable std::map<unsigned int, RunStats> m_processedRuns;

    /// The time the last calibration was run
    mutable time_t m_timeLastCalib{0};

    /// The time the current ongoing calibration started
    mutable time_t m_timeCurrentCalibStart{0};

    /// Publishing service
    IPublishSvc* m_pPublishSvc = nullptr;

    /// String to publish for new calibrations for RICH1
    mutable std::string m_Rich1PubString{""};

    /// String to publish for new calibrations for RICH2
    mutable std::string m_Rich2PubString{""};

    /// The RICH1 scale factor value to publish
    mutable double m_Rich1RefIndex{1.0};

    /// The RICH2 scale factor value to publish
    mutable double m_Rich2RefIndex{1.0};

    /// Current TCanvas for printing
    mutable std::unique_ptr<TCanvas> m_canvas;

    /// PDF file for each run
    mutable std::map<unsigned int, std::string> m_pdfFile;

    /** Keep a count of the number of calibrations in a row, per radiator,
     *  for which a cached value from a previous run is used */
    mutable std::unordered_map<std::string, unsigned int> m_cachedCalibCount;

    /// Map of condition version numbers for each run
    mutable std::unordered_map<std::string, std::unordered_map<unsigned int, unsigned long long>> m_runCalibVersions;

    /// The scale factors for the current data collection
    mutable ScaleFactorsCache m_scaleFs;

    /// Current failed fit count
    mutable RadiatorArray<unsigned int> m_failedFitCount{{0, 0, 0}};

  private:
    /// Fit Result object
    using FitResult = Rich::Rec::CKResolutionFitter::FitResult;

    /// Simple class to store a run number and saveset path
    class RunAndSaveSet {
    public:
      RunAndSaveSet() = default;
      RunAndSaveSet( const unsigned int r, const std::string& s ) : run( r ), saveset( s ) {}

    public:
      unsigned int run{0};
      std::string  saveset{""};
    };

    /// Set the currently used scale factors
    inline void setCurrentScaleFactors( const ScaleFactorsCache& scaleFs ) const noexcept { m_scaleFs = scaleFs; }

    // access the current scale factors cached in the writer
    inline const auto& currentScaleFactors() const noexcept { return m_scaleFs; }

    /// Get the current run number
    inline auto runNumber() const noexcept { return m_runNumber; }

    //=============================================================================
    /// Fit the histogram
    //=============================================================================
    FitResult fitCKThetaHistogram( TH1* hist, const std::string& rad ) const;

    //=============================================================================
    // Make a pull plot from a histogram and a given function
    //=============================================================================
    std::unique_ptr<TH1D> makePullPlot( TH1& hist, TF1& func ) const;

    //=============================================================================
    /// Function to check whether enough entries
    //=============================================================================
    inline bool checkCKThetaStats( const TH1* hist ) const {
      bool OK = false;
      if ( !hist ) {
        calib_message( MSG::WARNING, "checkCKThetaStats: NULL histogram !" );
      } else {
        OK = ( hist->GetEntries() >= m_minEntries );
        if ( !OK ) {
          calib_message( MSG::INFO, "CK theta histogram '", hist->GetTitle(), "' does not have enough stats for fit (",
                         hist->GetEntries(), "<", m_minEntries.value(), ")" );
        }
      }
      return OK;
    }

    //=============================================================================
    /// Function to calculate scale factor
    //=============================================================================
    inline auto nScaleFromShift( const double       shift,     //
                                 const double       shift_err, //
                                 const std::string& rad ) const {
      // which RICH
      const auto iR = ( "Rich1Gas" == rad ? Rich::Rich1 : Rich::Rich2 );
      // Calculate and return the scale factor
      const auto scaleFcorrection     = ( 1.0 + ( shift * m_RefIndexSlope[iR] ) );
      const auto newScale             = m_scaleFs[iR].value * scaleFcorrection;
      const auto scaleFcorrection_err = ( shift_err * m_RefIndexSlope[iR] );
      const auto newScale_err         = m_scaleFs[iR].value * scaleFcorrection_err;
      // apply a running average
      return createRunningScale( rad, ScaleFactor{newScale, newScale_err} );
    }

    //=============================================================================
    // Create running average of given scale factor
    //=============================================================================
    ScaleFactor createRunningScale( const std::string& rad, //
                                    const ScaleFactor  scale ) const;

    //=============================================================================
    // Function to get ref index scale from previous run
    //=============================================================================
    std::pair<double, std::time_t> getLastRefIndexSF( const std::string& rad ) const;

    //=============================================================================
    /// Function to update ref index scale
    //=============================================================================
    void updateRefIndexSF( const std::string& rad, //
                           const double       scale ) const;

    //=============================================================================
    /// Function to write out yml file
    //=============================================================================
    bool ymlWriter( const std::string& type,  //
                    const double       scale, //
                    const std::string& rad ) const;

    //=============================================================================
    /// Functions to get the last line
    //=============================================================================
    inline std::istream& ignoreline( std::fstream&           in, //
                                     std::fstream::pos_type& pos ) const {
      pos = in.tellg();
      return in.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
    }

    //=============================================================================
    /// Get the last line from an input file stream
    //=============================================================================
    inline auto getLastLine( std::fstream& in ) const {
      std::fstream::pos_type pos = in.tellg();
      std::fstream::pos_type lastPos;
      while ( in >> std::ws && ignoreline( in, lastPos ) ) { pos = lastPos; }
      in.clear();
      in.seekg( pos );
      std::string line;
      std::getline( in, line );
      return line;
    }

    //=============================================================================
    /// Function to set version number for current run
    //=============================================================================
    std::pair<bool, unsigned long long> setVersion( const std::string& rad ) const;

    /// Print Canvas
    void printCanvas( const std::string& tag = "" ) const;

    /// get DateTime string
    inline auto getDateTimeString( const char* fmt = "%Y%m%d:%H%M%S" ) const {
      const std::time_t t = std::time( nullptr );
      char              mbstr[100];
      return std::string{std::strftime( mbstr, sizeof( mbstr ), fmt, std::localtime( &t ) ) ? mbstr : ""};
    }

    /// Get year/month/day as a string
    inline auto getDateString() const { return getDateTimeString( "%Y/%m/%d" ); }

    /// Get watermark string
    inline auto watermark() const { return getDateTimeString( "%c" ); }

    /// Fix file permissions
    inline void fixFilePermissions() const {
      using namespace boost::filesystem;
      const path                   dir( m_summaryPath + "/RichCalibSummaries" );
      recursive_directory_iterator end_itr;
      for ( recursive_directory_iterator i( dir ); i != end_itr; ++i ) { setPerms( i->path() ); }
    }

    /// Set file permissions on a file
    template <class FILE>
    inline void setPerms( const FILE& file ) const {
      try {
        if ( boost::filesystem::exists( file ) ) {
          calib_message( MSG::VERBOSE, "Setting permissions for ", file );
          using namespace boost::filesystem;
          permissions( file, add_perms | owner_write | group_write );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::WARNING, "Failed to set permissions for ", file, " ", expt.what() );
      }
    }

    /// Remove a file
    template <class FILE>
    inline bool removeFile( const FILE& file ) const {
      bool ok = true;
      try {
        if ( boost::filesystem::exists( file ) ) {
          calib_message( MSG::DEBUG, "Removing ", file );
          boost::filesystem::remove( file );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to delete file ", file, " ", expt.what() );
        ok = false;
      }
      return ok;
    }

    /// Copy a file
    template <class FILEFROM, class FILETO>
    inline bool copyFile( const FILEFROM& from, const FILETO& to ) const {
      bool ok = true;
      try {
        ok = boost::filesystem::exists( from );
        if ( ok ) {
          calib_message( MSG::DEBUG, "Copying ", from, " -> ", to );
          ok = removeFile( to );
          if ( ok ) { boost::filesystem::copy_file( from, to ); }
        } else {
          calib_message( MSG::ERROR, "Failed to copy file ", from, " to ", to, " as source file does not exist" );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to copy file ", from, " to ", to, " ", expt.what() );
        ok = false;
      }
      return ok;
    }

    /// Create directory
    template <class DIR>
    inline bool createDir( const DIR& dir, const bool errorOnFail = true ) const {
      bool ok = true;
      try {
        if ( !boost::filesystem::exists( dir ) ) {
          boost::filesystem::create_directories( dir );
          calib_message( MSG::DEBUG, "Created ", dir );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        if ( errorOnFail ) { calib_message( MSG::ERROR, "FAILED to create directory ", dir, " '", expt.what(), "'" ); }
        ok = false;
      }
      return ok;
    }

    /// Access and create on demand the current TCanvas
    inline auto canvas() const {
      if ( !m_canvas.get() ) {
        const std::string name = "RefractiveIndexCanvas";
        calib_message( MSG::VERBOSE, "Creating TCanvas '", name, "'" );
        m_canvas = std::make_unique<TCanvas>( name.c_str(), name.c_str(), 1200, 1200 );
      }
      return m_canvas.get();
    }

    /// Delete the current TCanvas
    inline void deleteCanvas() const {
      if ( m_canvas.get() ) { calib_message( MSG::VERBOSE, "Deleting TCanvas '", m_canvas->GetName(), "'" ); }
      m_canvas.reset( nullptr );
    }

    /// Get the path to write summaries to
    inline auto summaryPath() const {
      const std::string dir( m_summaryPath + "/RichCalibSummaries/" + getDateString() + "/RefIndex/" );
      createDir( dir );
      return dir;
    }

    /// Print a divider
    inline void divider() const noexcept {
      calib_message( MSG::INFO, "=======================================================",
                     "===============================================================" );
    }

    /// DIM summary file location
    inline std::string dimSummaryFile() const { return summaryPath() + m_dimSummaryFile; }

    /// logger file location
    inline std::string loggerFile() const { return summaryPath() + m_loggerFile; }

    /// Write a message to the DIM summary file
    bool writeToDimSummaryFile( const std::string& mess ) const;

    /// Write a message to the logger file
    bool writeToLoggerFile( const MSG::Level level, const std::string& mess ) const;

    /// Write summary logs
    bool writeLogs( const std::string& rad, //
                    const FitResult&   fitResult ) const;

    /// Radiator name to task name
    inline auto radToTaskName( const std::string& rad ) const noexcept {
      const std::string task = ( "Rich1Gas" == rad ? "Rich1/Calib" :     //
                                     "Rich2Gas" == rad ? "Rich2/Calib" : //
                                         "" );
      if ( task.empty() ) { calib_message( MSG::WARNING, "Unknown radiator '", rad, "'" ); }
      return task;
    }

  private:
    // messaging

    template <typename... Args>
    inline void calib_message( const MSG::Level level, Args&&... args ) const {
      if ( msgLevel( level ) ) {
        std::ostringstream mess;
        ( mess << ... << args );
        msgStream( level ) << mess.str() << endmsg;
        // Only write to log file if publishing enabled.
        if ( m_okToPublish ) { writeToLoggerFile( level, mess.str() ); }
      }
    }
  };

} // namespace Rich::Future::Rec::Calib

using namespace Rich::Future::Rec::Calib;

//-----------------------------------------------------------------------------

void RefIndexCalib::operator()( const LHCb::ODIN&                         odin,          //
                                const LHCb::Track::Selection&             tracks,        //
                                const Summary::Track::Vector&             sumTracks,     //
                                const Relations::PhotonToParents::Vector& photToSegPix,  //
                                const LHCb::RichTrackSegment::Vector&     segments,      //
                                const CherenkovAngles::Vector&            expTkCKThetas, //
                                const SIMDCherenkovPhoton::Vector&        photons,       //
                                const ScaleFactorsCache&                  scaleFactors ) const {

  // Only one thread can be running from this point on
  std::scoped_lock lock{m_mutex};

  // First event printout
  if ( 0 == m_nEvts ) {
    calib_message( MSG::INFO, "First Event Seen" );
    // set last time to now.
    m_timeLastCalib = time( nullptr );
  }

  // Current run number
  const auto RunNumber = odin.runNumber();

  // Send info on new runs
  if ( RunNumber != runNumber() ) {
    // check to see if this is one we have seen in the past.
    // protects against 'flip flopping' run numbers online that can be
    // seen at run changes.
    auto runStats = m_processedRuns.find( RunNumber );
    if ( runStats == m_processedRuns.end() ) {
      calib_message( MSG::INFO, "==================================================> New Run ", RunNumber,
                     " <==================================================" );
    } else {
      // Have we gone back in time... ?
      if ( RunNumber < runNumber() ) {
        ++runStats->second.late;
        return;
      }
    }
    // before updating cached run number, check if new run-by-run histograms are required
    if ( m_enableRunByRunHists ) {
      calib_message( MSG::DEBUG, "Resetting run-by-run histograms for run ", RunNumber );
      const auto runS = std::to_string( RunNumber );
      for ( const auto rad : activeRadiators() ) {
        // reset pointers to new histos for this run
        h_ckThetaRec_run[rad] = richHisto1D( HID( "Runs/" + runS + "/ckThetaRec", rad ), "Rec CKTheta | Run " + runS, //
                                             m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), "Cherenkov theta / rad" );
        h_ckThetaExp_run[rad] = richHisto1D( HID( "Runs/" + runS + "/ckThetaExp", rad ), "Exp CKTheta | Run " + runS, //
                                             m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), "Cherenkov theta / rad" );
        h_ckThetaRes_run[rad] = richHisto1D( HID( "Runs/" + runS + "/ckThetaRes", rad ),       //
                                             "Rec-Exp CKTheta | Run " + runS,                  //
                                             -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                             "delta(Cherenkov theta) / rad" );
        h_tkPtot_run[rad]     = richHisto1D( HID( "Runs/" + runS + "/tkPtot", rad ), "Track Momentum | Run " + runS, //
                                         m_minP[rad], std::min( m_maxP[rad], 120 * Gaudi::Units::GeV ), nBins1D(),
                                         "Track Momentum / MeV" );
        h_ckThetaRes_quadrants_run[rad][0] = richHisto1D( HID( "Runs/" + runS + "/ckThetaResQuad0", rad ),         //
                                                          "Rec-Exp CKTheta | Run " + runS + " | Quadrant x>0 y>0", //
                                                          -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),        //
                                                          "delta(Cherenkov theta) / rad" );
        h_ckThetaRes_quadrants_run[rad][1] = richHisto1D( HID( "Runs/" + runS + "/ckThetaResQuad1", rad ),         //
                                                          "Rec-Exp CKTheta | Run " + runS + " | Quadrant x<0 y>0", //
                                                          -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),        //
                                                          "delta(Cherenkov theta) / rad" );
        h_ckThetaRes_quadrants_run[rad][2] = richHisto1D( HID( "Runs/" + runS + "/ckThetaResQuad2", rad ),         //
                                                          "Rec-Exp CKTheta | Run " + runS + " | Quadrant x>0 y<0", //
                                                          -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),        //
                                                          "delta(Cherenkov theta) / rad" );
        h_ckThetaRes_quadrants_run[rad][3] = richHisto1D( HID( "Runs/" + runS + "/ckThetaResQuad3", rad ),         //
                                                          "Rec-Exp CKTheta | Run " + runS + " | Quadrant x<0 y<0", //
                                                          -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),        //
                                                          "delta(Cherenkov theta) / rad" );
        // track (x,y) plots
        const double xSize   = ( Rich::Rich1Gas == rad ? 500 : 2000 );
        const double ySize   = ( Rich::Rich1Gas == rad ? 500 : 2000 );
        h_tkEntryXY_run[rad] = richHisto2D( HID( "Runs/" + runS + "/trackEntryXY", rad ), //
                                            "Track Radiator Entry (x,y) | Run " + runS,   //
                                            -xSize, xSize, nBins2D(),                     //
                                            -ySize, ySize, nBins2D(),                     //
                                            "Track Entry X / mm", "TrackEntry Y / mm" );
      }
    }
  }

  // Check to see if a calibration should be run due to a new run being detected
  if ( runNumber() != 0 ) {
    if ( runNumber() != RunNumber ) {
      runCalibration();
    } else if ( ( currentScaleFactors()[Rich::Rich1].value != scaleFactors[Rich::Rich1].value ||
                  currentScaleFactors()[Rich::Rich2].value != scaleFactors[Rich::Rich2].value ) &&
                m_nEvts > 0 ) {
      // scale factors have changed but no run change ?
      calib_message( MSG::WARNING, "Scale factors changed for run ", RunNumber, " | Old=", currentScaleFactors(),
                     " New=", scaleFactors );
      runCalibration();
    }
  }

  // update cached run number
  m_runNumber = RunNumber;
  // count events
  ++m_nEvts;

  // update cached scale factors in writer for current run
  setCurrentScaleFactors( scaleFactors );

  // run periodic calibration ?
  if ( m_nEventsSinceCalib >= m_minEventsForCalib && m_minTimeBetweenCalibs > 0 ) {
    // time since last calibration
    const time_t deltaT = ( time( nullptr ) - m_timeLastCalib );
    if ( deltaT >= m_minTimeBetweenCalibs ) {
      // run periodic calibration, no histogram reset...
      runCalibration( "Periodic" );
    }
  }

  // run a fit retry due to a previous failure ?
  if ( canRetryFit() ) {
    // time since last calibration
    const time_t deltaT = ( time( nullptr ) - m_timeLastCalib );
    if ( deltaT >= m_failedFitRetryPeriod ) { runCalibration( "Retry" ); }
  }

  // count events since last calibration was made
  ++m_nEventsSinceCalib;

  // count events this run
  ++m_nEventsThisRun;

  // =================================================================================
  // Fill the histograms
  // =================================================================================

  // The PID type to assume. Just use Pion here.
  const auto pid = Rich::Pion;

  // loop over tracks
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      if ( beta < m_minBeta[rad] || beta > m_maxBeta[rad] ) { continue; }

      // expected CK theta in SIMD form
      const SIMDCherenkovPhoton::SIMDFP thetaExp( expCKangles[pid] );

      // SIMD delta theta
      const auto deltaTheta = phot.CherenkovTheta() - thetaExp;

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries and fill
        if ( phot.validityMask()[i] ) { h_ckResAll[rad]->Fill( deltaTheta[i] ); }
      }

      // detailed run by run plots
      if ( m_enableRunByRunHists ) {
        // Segment (x,y) entry point to radiator
        const auto seg_x = seg.entryPoint().x();
        const auto seg_y = seg.entryPoint().y();
        fillHisto( h_tkEntryXY_run[rad], seg_x, seg_y );
        // Loop over scalar entries in SIMD photon
        for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
          // Select valid entries and fill
          if ( phot.validityMask()[i] ) {
            fillHisto( h_ckThetaRec_run[rad], phot.CherenkovTheta()[i] );
            fillHisto( h_ckThetaExp_run[rad], thetaExp[i] );
            fillHisto( h_ckThetaRes_run[rad], deltaTheta[i] );
            if ( seg_x > 0 ) {
              if ( seg_y > 0 ) {
                fillHisto( h_ckThetaRes_quadrants_run[rad][0], deltaTheta[i] );
              } else {
                fillHisto( h_ckThetaRes_quadrants_run[rad][2], deltaTheta[i] );
              }
            } else {
              if ( seg_y > 0 ) {
                fillHisto( h_ckThetaRes_quadrants_run[rad][1], deltaTheta[i] );
              } else {
                fillHisto( h_ckThetaRes_quadrants_run[rad][3], deltaTheta[i] );
              }
            }
          }
        }
      }

    } // photons
  }   // tracks

  // detailed run by run plots
  if ( m_enableRunByRunHists ) {
    for ( const auto& seg : segments ) {
      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }
      // Segment momentum
      const auto pTot = seg.bestMomentumMag();
      // beta
      const auto beta = richPartProps()->beta( pTot, pid );
      if ( beta < m_minBeta[rad] || beta > m_maxBeta[rad] ) { continue; }
      fillHisto( h_tkPtot_run[rad], pTot );
    }
  }
}

void RefIndexCalib::runCalibration( const std::string& type ) const {

  // check run number is reasonable
  if ( 0 == runNumber() ) {
    // if we have also seen events, something is wrong...
    if ( m_nEventsSinceCalib > 0 ) {
      calib_message( MSG::WARNING, "Undefined run number !! -> ", type, " calibration aborted" );
    }
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Only do anything if some events have been seen since last time.
  if ( 0 == m_nEventsSinceCalib ) {
    calib_message( MSG::WARNING, "No events processed for run ", runNumber(), " -> ", type, " calibration aborted" );
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Is this a previously calibrated run
  const auto iRunCalData = m_processedRuns.find( runNumber() );
  if ( iRunCalData != m_processedRuns.end() ) {
    // Do we now have more events to calibrate with than last time ?
    if ( iRunCalData->second.used >= m_nEventsThisRun ) {
      calib_message( MSG::DEBUG, "Already calibrated run ", runNumber(), " -> Processing skipped" );
      resetCalibCounters();
      return;
    } else {
      calib_message( MSG::INFO, "Run ", runNumber(), " : Previous #Events = ", iRunCalData->second.used,
                     " New #Events = ", m_nEventsThisRun );
    }
  }

  // Print message on the number of events seen and the rate
  // current time
  m_timeCurrentCalibStart = time( nullptr );
  // time since last calibration
  const time_t deltaT = ( m_timeCurrentCalibStart - m_timeLastCalib );
  if ( deltaT > 0 ) {
    struct tm*         timeinfo = localtime( &deltaT );
    std::ostringstream messageS;
    messageS << "Seen " << m_nEventsSinceCalib << " events in past ";
    if ( timeinfo->tm_hour - 1 > 0 ) { messageS << timeinfo->tm_hour - 1 << " hours "; }
    if ( timeinfo->tm_min > 0 ) { messageS << timeinfo->tm_min << " mins "; }
    messageS << timeinfo->tm_sec << " secs ";
    messageS << "( " << (double)( m_nEventsSinceCalib ) / (double)( deltaT ) << " Evt/s )";
    calib_message( MSG::INFO, messageS.str() );
  }

  calib_message( MSG::INFO, "Starting ", type, " n-1 calibration for run ", runNumber() );

  bool ok           = true;
  bool canvasIsOpen = false;

  // calibration type
  const bool isFinal = ( type == "Final EOR" || type == "Final" );
  const bool isRetry = ( type == "Retry" );

  // Loop over radiators
  for ( const auto& rad : m_rads ) {

    // which RICH and Rad
    const auto iRich = ( "Rich1Gas" == rad ? Rich::Rich1 : Rich::Rich2 );
    const auto iRad  = ( "Rich1Gas" == rad ? Rich::Rich1Gas : Rich::Rich2Gas );

    // is this a retry ?
    if ( isRetry && !canRetryFit( iRad ) ) {
      calib_message( MSG::INFO, "No fit retry needed for ", rad );
      continue;
    }

    calib_message( MSG::DEBUG, "Attempting to calibrate ", rad );

    // try and load the histogram
    auto radHist = h_ckResAll[iRad];
    if ( radHist ) {

      // Make sure axis titles are set..
      if ( radHist->GetXaxis() ) { radHist->GetXaxis()->SetTitle( "delta(Cherenkov Theta) / rad" ); }
      if ( radHist->GetYaxis() ) { radHist->GetYaxis()->SetTitle( "Entries" ); }

      // save fit result;
      FitResult savedFitResult;

      // default scale same as condition value for current run
      ScaleFactor scale = m_scaleFs[iRich];

      bool hasNewScaleF = false;

      if ( checkCKThetaStats( radHist ) ) {

        // If need be open output file
        if ( !canvasIsOpen ) {
          printCanvas( "[" );
          canvasIsOpen = true;
        }

        // Draw to canvas
        radHist->Draw( "" );

        // run the fit
        const auto fitresult = fitCKThetaHistogram( radHist, rad );
        if ( fitresult.fitOK ) {
          savedFitResult          = fitresult;
          m_cachedCalibCount[rad] = 0; // Normal calibration, so reset count to 0
          // compute scale factor from fitted shift
          scale = nScaleFromShift( fitresult.ckShift, fitresult.ckShiftErr, rad );
          calib_message( MSG::INFO, rad, " Scale Factor | Current ", m_scaleFs[iRich], " -> Update ", scale );
          // reset failed fit count
          resetFailedfitCounts( iRad );
          hasNewScaleF = true;
        } else {
          ++m_cachedCalibCount[rad]; // Count cached calibrations
          scale = ScaleFactor{getLastRefIndexSF( rad ).first, 0.0};
          calib_message( MSG::WARNING, "Fit FAILED. Using scale from previous run ", scale );
          // increment failed fit count for this radiator
          ++m_failedFitCount[iRad];
          calib_message( MSG::WARNING, "Incremented ", rad, " failed fit count to ", m_failedFitCount[iRad] );
          if ( m_failedFitCount[iRad] >= m_maxfailedFitRetries ) {
            calib_message( MSG::WARNING, "Maximum number of fit retries reached for ", rad,
                           ". No more will be attempted" );
          }
        }
      } else {
        // not enough statistics, take the one from previous run
        ++m_cachedCalibCount[rad]; // Count cached calibrations
        scale = ScaleFactor{getLastRefIndexSF( rad ).first, 0.0};
        calib_message( MSG::INFO, "Not enough statistics for ", rad, " -> Using scale from previous run ", scale );
      }

      // if a final OK calibration, write to CK resolution and scale summary
      if ( savedFitResult.fitOK && isFinal && m_okToPublish ) { ok &= writeLogs( rad, savedFitResult ); }

      // publish results. Only if histogram did not have 0 entries
      if ( 0 != radHist->GetEntries() ) {
        if ( m_cachedCalibCount[rad] > m_maxCachedCalibs ) {
          calib_message( MSG::WARNING, "Too many (", m_cachedCalibCount[rad], ") cached calibrations in a row." );
        }
        ok = ymlWriter( type, scale.value, rad );
        if ( !ok ) { calib_message( MSG::WARNING, "Failed to write YML file for ", rad ); }
      } else {
        calib_message( MSG::INFO, "YML Writing is DISABLED for this dataset" );
      }

      calib_message( MSG::DEBUG, "Scale factor for ", rad, ": ", scale );

      // Update cached scale factor for 'last' run if a final fit and OK
      if ( hasNewScaleF && isFinal ) { updateRefIndexSF( rad, scale.value ); }

    } else {
      calib_message( MSG::WARNING, "Failed to load histogram for '", rad, "'" );
    }

  } // Loop over radiators

  if ( canvasIsOpen ) { printCanvas( "]" ); }

  if ( ok ) {
    // save this run in the processed list with number of events used.
    m_processedRuns[runNumber()].used = m_nEventsThisRun;
  }

  calib_message( MSG::INFO, "Finished ", type, " n-1 calibration for run ", runNumber() );

  // reset various counters for the next run
  resetCalibCounters();
  if ( isFinal ) { resetHistograms(); }

  // Make sure we finally remove any canvas created
  deleteCanvas();
}

//=============================================================================

ScaleFactor RefIndexCalib::createRunningScale( const std::string& rad, //
                                               ScaleFactor        scale ) const {

  if ( m_useRunningAv.value() ) {

    const auto last_scale = getLastRefIndexSF( rad );

    const auto time_delta = m_timeCurrentCalibStart - last_scale.second;
    // just in case ...
    if ( time_delta <= 0 ) { return scale; }

    // Weight for past scale factor
    // 0.7 for 10 mins old
    // 0.25 for 60 mins old
    // linearly decreasing for older times.
    // No larger than 0.7, no smaller than 0
    auto weight = []( const auto dt ) {
      const std::time_t t1 = 10 * 60;
      const std::time_t t2 = 60 * 60;
      const double      w1 = 0.70;
      const double      w2 = 0.25;
      const double      m  = ( w1 - w2 ) / ( t1 - t2 );
      const double      c  = ( ( w2 * t1 ) - ( w1 * t2 ) ) / ( t1 - t2 );
      return std::clamp( ( ( m * dt ) + c ), 0.0, std::max( w1, w2 ) );
    };

    // Form new running scale from weighted average
    const auto w         = weight( time_delta );
    const auto new_scale = ( w * last_scale.first ) + ( ( 1.0 - w ) * scale.value );

    calib_message( MSG::INFO, rad, " Running Scale | Last=", last_scale.first, " dT=", time_delta, " weight=", w,
                   " New=", scale.value, " | WeightedAv=", new_scale );

    scale.value = new_scale;
  }

  return scale;
}

//=============================================================================

std::pair<double, std::time_t> RefIndexCalib::getLastRefIndexSF( const std::string& rad ) const {

  std::pair<double, std::time_t> scale_time{0, 0};
  auto&                          scale      = scale_time.first;
  auto&                          write_time = scale_time.second;
  unsigned int                   run        = 0;

  const auto subDet = radToTaskName( rad );
  if ( !subDet.empty() ) {
    const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
    const boost::filesystem::path filename( m_RefIndexSFLog.value() );
    const boost::filesystem::path full_path = dir / filename;
    if ( !boost::filesystem::exists( full_path ) ) {
      // first time write new file
      try {
        auto ok = createDir( dir );
        if ( ok ) {
          std::ofstream logging( full_path.string().c_str() );
          ok = logging.is_open();
          if ( ok ) {
            logging << run << " " << std::setprecision( m_Precision ) << scale << "\n";
            logging.close();
            // set write time to 'now'
            write_time = time( nullptr );
          } else {
            calib_message( MSG::ERROR, "Failed to open ", full_path );
          }
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to create file ", full_path, " ", expt.what() );
      }

    } else {
      // open existing file

      // Get last write time
      write_time = boost::filesystem::last_write_time( full_path );

      std::fstream file( full_path.string().c_str(), std::ios::in );
      if ( file.is_open() ) {
        file >> run >> scale;
        file.close();
      } else {
        calib_message( MSG::ERROR, "Failed to open ", full_path );
      }

      calib_message( MSG::DEBUG, "Read ", rad, " scale factor (", scale, ") from previous run cache" );
    }
  }

  return scale_time;
}

//=============================================================================

void RefIndexCalib::updateRefIndexSF( const std::string& rad, //
                                      const double       scale ) const {

  if ( m_okToPublish ) {
    const auto subDet = radToTaskName( rad );
    bool       OK     = !subDet.empty();
    if ( OK ) {
      const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
      const boost::filesystem::path filename( m_RefIndexSFLog.value() );
      const boost::filesystem::path full_path = dir / filename;
      try {
        OK = createDir( dir );
        if ( OK ) {
          std::ofstream ofile( full_path.string().c_str() );
          OK = ofile.is_open();
          if ( OK ) {
            ofile << runNumber() << " " << std::setprecision( m_Precision ) << scale << "\n";
            ofile.close();
            calib_message( MSG::DEBUG, "Cached ", rad, " scale factor ", scale, " for run ", runNumber() );
          } else {
            calib_message( MSG::ERROR, "Failed to open ", full_path );
          }
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to open file ", full_path, " ", expt.what() );
        OK = false;
      }
    }
    if ( !OK ) { calib_message( MSG::WARNING, "Failed to update ", rad, " ref index scale" ); }
  }
}

//=============================================================================

bool RefIndexCalib::ymlWriter( const std::string& type,  //
                               const double       scale, //
                               const std::string& rad ) const {

  // Only write YML if we are going to publish the result
  if ( !m_okToPublish ) { return false; }

  // Get version
  const auto version = setVersion( rad );
  bool       OK      = version.first;
  if ( !OK ) { return OK; }
  calib_message( MSG::DEBUG, "YML Writing for run ", runNumber(), " ", rad, " Version ", version.second );
  const auto versionS = std::to_string( version.second );

  const bool isR1             = ( "Rich1Gas" == rad );
  const bool isR2             = ( "Rich2Gas" == rad );
  const bool isFinal          = ( type == "Final EOR" || type == "Final" );
  bool       scaleIsDifferent = false;

  std::string subDet;
  if ( isR1 ) {
    subDet           = "Rich1";
    m_Rich1PubString = std::to_string( runNumber() ) + " v" + versionS;
    scaleIsDifferent = m_Rich1RefIndex != scale;
    m_Rich1RefIndex  = scale;
  } else if ( isR2 ) {
    subDet           = "Rich2";
    m_Rich2PubString = std::to_string( runNumber() ) + " v" + versionS;
    scaleIsDifferent = m_Rich2RefIndex != scale;
    m_Rich2RefIndex  = scale;
  } else {
    calib_message( MSG::WARNING, "Unknown radiator '", rad, "'" );
    return false;
  }

  const boost::filesystem::path conditions_dir( m_conditionsDbPath + "/lhcb-conditions-database/Conditions/" + subDet +
                                                "/Environment/Gas.yml" );
  const boost::filesystem::path pool_dir = conditions_dir / ".pool";
  const boost::filesystem::path conditions_file( "v" + versionS );
  const boost::filesystem::path full_conditions_path = pool_dir / conditions_file;

  calib_message( MSG::VERBOSE, "Writing conditions to ", full_conditions_path );

  removeFile( full_conditions_path );

  try {
    OK = createDir( pool_dir );
    if ( OK ) {
      std::ofstream logging( full_conditions_path.string().c_str() );
      OK = logging.is_open();
      if ( OK ) {
        logging << "RefractivityScaleFactor:\n";
        logging << "  CurrentScaleFactor: " << std::setprecision( m_Precision ) << scale << "\n";
        logging.close();
        calib_message( MSG::INFO, "Successfully wrote ", rad, " Scale factor to ", full_conditions_path );
        if ( m_makeDBRunFile ) {
          const boost::filesystem::path run_db_file = conditions_dir / std::to_string( runNumber() );
          removeFile( run_db_file );
          copyFile( full_conditions_path, run_db_file );
        }
      } else {
        calib_message( MSG::ERROR, "Failed to open ", full_conditions_path );
      }
    }
  } catch ( const boost::filesystem::filesystem_error& expt ) {
    calib_message( MSG::ERROR, "Failed to open file ", full_conditions_path, " ", expt.what() );
    OK = false;
  }

  // Publish to DIM
  if ( OK ) {
    if ( m_pPublishSvc ) {
      if ( isR1 ) {
        calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich1TaskName.value(), " = '", m_Rich1PubString, "'" );
        m_pPublishSvc->updateItem( m_Rich1TaskName.value() );
        if ( isFinal && scaleIsDifferent ) {
          calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich1ScaleTaskName.value(), " = ", m_Rich1RefIndex );
          m_pPublishSvc->updateItem( m_Rich1ScaleTaskName.value() );
        }
      } else if ( isR2 ) {
        calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich2TaskName.value(), " = '", m_Rich2PubString, "'" );
        m_pPublishSvc->updateItem( m_Rich2TaskName.value() );
        if ( isFinal && scaleIsDifferent ) {
          calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich2ScaleTaskName.value(), " = ", m_Rich2RefIndex );
          m_pPublishSvc->updateItem( m_Rich2ScaleTaskName.value() );
        }
      }
    }
    // update DIM summary file
    std::ostringstream dimmess;
    dimmess << "Run " << runNumber() << " |";
    if ( isR1 ) { dimmess << " " << m_Rich1TaskName.value() << " '" << m_Rich1PubString << "'"; }
    if ( isR2 ) { dimmess << " " << m_Rich2TaskName.value() << " '" << m_Rich2PubString << "'"; }
    OK = writeToDimSummaryFile( dimmess.str() );
  } else {
    calib_message( MSG::ERROR, rad, " YML conditions NOT published for Run ", runNumber() );
  }

  return OK;
}

//=============================================================================

bool RefIndexCalib::writeToDimSummaryFile( const std::string& mess ) const {
  bool OK = true;
  if ( m_okToPublish ) {
    // Open the summary file in append mode
    std::ofstream file( dimSummaryFile().c_str(), std::ios_base::app );
    OK = file.is_open();
    if ( OK ) {
      file << getDateTimeString( "%Y-%m-%d %H:%M:%S" ) << " | " << mess << std::endl;
      file.close();
      // setPerms( dimSummaryFile() );
    } else {
      calib_message( MSG::WARNING, "Problem writing DIM summary file ", dimSummaryFile() );
    }
  }
  return OK;
}

//=============================================================================

bool RefIndexCalib::writeToLoggerFile( const MSG::Level level, const std::string& mess ) const {
  // Open the summary file in append mode
  std::ofstream file( loggerFile().c_str(), std::ios_base::app );
  const bool    OK = file.is_open();
  if ( OK ) {
    file << getDateTimeString( "%Y-%m-%d %H:%M:%S" ) << " | ";
    if ( MSG::INFO == level ) {
      file << "INFO  | ";
    } else if ( MSG::WARNING == level ) {
      file << "WARN  | ";
    } else if ( MSG::ERROR == level ) {
      file << "ERROR | ";
    } else if ( MSG::FATAL == level ) {
      file << "FATAL | ";
    } else if ( MSG::DEBUG == level ) {
      file << "DEBUG | ";
    } else if ( MSG::VERBOSE == level ) {
      file << "VERBO | ";
    } else if ( MSG::ALWAYS == level ) {
      file << "ALWYS | ";
    }
    file << mess << std::endl;
    file.close();
  } else {
    // do not convert to calib_message( MSG::WARNING
    warning() << "Problem writing logger summary file " << dimSummaryFile() << endmsg;
  }
  return OK;
}

//=============================================================================

bool RefIndexCalib::writeLogs( const std::string& rad, //
                               const FitResult&   fitResult ) const {

  // Path to the summary file
  const boost::filesystem::path dir( m_summaryPath + "/RichCalibSummaries/" );
  const boost::filesystem::path filename( rad + m_runByrunLog );
  const boost::filesystem::path full_path = dir / filename;

  bool ok = createDir( dir );
  if ( ok ) {

    // First time ?
    const bool firstTime = !boost::filesystem::exists( full_path );

    // open the file in append mode
    std::ofstream file( full_path.string().c_str(), std::ios_base::app );
    ok = file.is_open();
    if ( ok ) {

      // Add column headers if first time
      if ( firstTime ) {
        file << "Run DateTime CKThetaRes CKThetaResErr ScaleFactor ScaleFactorErr NSignal NBackground" << std::endl;
      }

      // get DateTime string
      const auto dateTime = getDateTimeString();

      // get the scale parameter
      const auto scale = nScaleFromShift( fitResult.ckShift, fitResult.ckShiftErr, rad );

      // get S and B
      const auto& sigF = fitResult.signalFitFunc;
      const auto& bckF = fitResult.bkgFitFunc;
      const auto  x    = fitResult.ckShift;
      const auto  S    = ( sigF.get() ? sigF->Eval( x ) : 0.0 );
      const auto  B    = ( bckF.get() ? bckF->Eval( x ) : 0.0 );

      // write the data for this run
      file << runNumber() << " " << dateTime << " " << fitResult.ckResolution << " " << fitResult.ckResolutionErr //
           << " " << scale.value << " " << scale.error << " " << S << " " << B << std::endl;

      // close the file
      file.close();

    } else {
      calib_message( MSG::WARNING, "Problem writing run by run summary file ", full_path );
    }
  }

  return ok;
}

//=============================================================================

std::pair<bool, unsigned long long> //
RefIndexCalib::setVersion( const std::string& rad ) const {

  // Do we already have this run in the map
  // if so we reuse the conditions version to avoid creating too many files
  const auto iV = m_runCalibVersions[rad].find( runNumber() );
  if ( iV != m_runCalibVersions[rad].end() ) {
    calib_message( MSG::INFO, "Re-using ", rad, " conditions version ", iV->second, " for Run ", runNumber() );
    return std::make_pair( true, iV->second );
  }

  unsigned long long version = 0;

  // Test the root directory is accessible
  bool OK = createDir( m_summaryPath.value() );
  if ( !OK ) {
    calib_message( MSG::ERROR, "Cannot access alignment directory ", m_summaryPath );
  } else {

    const auto subDet = radToTaskName( rad );
    OK                = !subDet.empty();
    if ( OK ) {

      const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
      const boost::filesystem::path filename( m_ymlVersionLog.value() );
      const boost::filesystem::path full_path = dir / filename;

      unsigned int run( 0 );

      calib_message( MSG::VERBOSE, "Version file: ", full_path.string() );

      if ( !boost::filesystem::exists( full_path ) ) {
        // First time file is created
        try {
          version = 0;
          OK      = createDir( dir );
          if ( OK ) {
            std::ofstream logging( full_path.string().c_str() );
            OK = logging.is_open();
            if ( OK ) {
              logging << runNumber() << " " << version << "\n";
              logging.close();
            } else {
              calib_message( MSG::ERROR, "Failed to open ", full_path );
            }
          }
        } catch ( const boost::filesystem::filesystem_error& expt ) {
          calib_message( MSG::ERROR, "Failed to open file ", full_path, " ", expt.what() );
          OK = false;
        }

      } else {
        // reuse existing file
        std::fstream file( full_path.string().c_str(), std::ios::in | std::ios::out );
        if ( file.is_open() ) {
          std::istringstream lastLine( getLastLine( file ) );

          lastLine >> run >> version;

          if ( runNumber() > run ) {
            // increase version number and add to cache
            ++version;
            file << runNumber() << " " << version << "\n";
          }

          file.close();
        } else {
          OK = false;
          calib_message( MSG::ERROR, "Failed to open ", full_path );
        }
      }
    }

    // Save in map
    calib_message( MSG::INFO, "Setting ", rad, " conditions version ", version, " for Run ", runNumber() );
    m_runCalibVersions[rad][runNumber()] = version;
  }

  return std::make_pair( OK, version );
}

//=============================================================================

void RefIndexCalib::printCanvas( const std::string& tag ) const {

  if ( !m_createPDFsummary ) { return; }

  const std::string imageType = "pdf";

  // get the PDF file name for this run
  auto& pdfFile = m_pdfFile[runNumber()];

  // Opening file ?
  if ( "[" == tag ) {

    // If file name is not empty (so repeat fit for a run), delete file
    if ( !pdfFile.empty() ) { removeFile( pdfFile ); }

    // Directory to save summaries to
    const boost::filesystem::path dir( m_summaryPath + "/RichCalibSummaries/" + getDateString() + "/RefIndex/" );

    // Create directory if it does not exist
    const bool ok = createDir( dir );
    if ( ok ) {

      // File name
      const boost::filesystem::path filename( "Run-" + std::to_string( runNumber() ) + "." + imageType );
      const boost::filesystem::path full_path = dir / filename;

      // If PDF exists, remove before remaking
      removeFile( full_path );

      // cache image file name for this run
      pdfFile = full_path.string();

      // Make a new canvas
      deleteCanvas();

      // Open new PDF
      canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

    } else {
      calib_message( MSG::ERROR, "Failed to create PDF file ", pdfFile );
      deleteCanvas();
      pdfFile = "";
    }

  }
  // Closing the file ?
  else if ( "]" == tag ) {

    // Close the file
    canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

    // Set group write permissions
    // setPerms( pdfFile );

    // send a message
    calib_message( MSG::INFO, "Created  ", pdfFile );

    // delete canvas
    deleteCanvas();

    // limit the PDF file name map to N entries
    while ( m_pdfFile.size() > m_runNumHistSize + 10 ) { m_pdfFile.erase( m_pdfFile.begin() ); }

  } else if ( "" == tag ) {

    // Add time/date watermark
    canvas()->cd();
    TText text;
    text.SetNDC();
    text.SetTextSize( 0.012 );
    text.SetTextColor( 13 );
    text.DrawText( 0.01, 0.01, watermark().c_str() );

    // Just print
    canvas()->Print( pdfFile.c_str(), imageType.c_str() );

  } else if ( "DELETE" == tag ) {
    if ( !pdfFile.empty() ) { removeFile( pdfFile ); }
  }
}

//=============================================================================
// Fitting histogram
//=============================================================================
RefIndexCalib::FitResult //
RefIndexCalib::fitCKThetaHistogram( TH1* hist, const std::string& rad ) const {

  calib_message( MSG::DEBUG, "Starting CK theta fit for ", rad );

  const auto irad  = ( "Rich1Gas" == rad ? Rich::Rich1Gas : Rich::Rich2Gas );
  const auto irich = ( "Rich1Gas" == rad ? Rich::Rich1 : Rich::Rich2 );

  RefIndexCalib::FitResult fitRes;

  // Do the fit
  try {
    fitRes = m_ckFitter.fit( *hist, irad );
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception whilst performing CK theta fit '", excpt.what(), "'" );
    fitRes = RefIndexCalib::FitResult{};
  }

  // shortcuts to the results
  const auto& fitOK    = fitRes.fitOK;
  auto&       bestFitF = fitRes.overallFitFunc;
  auto&       backFunc = fitRes.bkgFitFunc;
  auto&       sigFunc  = fitRes.signalFitFunc;
  if ( !bestFitF.get() || !backFunc.get() || !sigFunc.get() ) {
    calib_message( MSG::ERROR, "NULL function pointer in fit result" );
    fitRes = RefIndexCalib::FitResult{};
    return fitRes;
  }

  // where multi attempts used by the fitter ?
  if ( fitRes.fitAttempts > 1 ) {
    calib_message( MSG::WARNING, "CK theta fitter used multiple (", fitRes.fitAttempts, ") fit attempts" );
  }

  // randomly fail fits for testing
  // if ( fitOK ) { fitOK = ( int( bestFitF.GetParameter( 0 ) ) % 2 == 0 ); }

  // print parameters
  try {
    if ( fitOK ) {
      // Print fitted parameters
      calib_message( MSG::INFO, rad, " fit SUCCESSFUL :-" );
      for ( int i = 0; i < bestFitF->GetNpar(); ++i ) {
        const auto name  = bestFitF->GetParName( i );
        const auto param = bestFitF->GetParameter( i );
        const auto err   = bestFitF->GetParError( i );
        calib_message( MSG::INFO, boost::format( "  %s %|15t|= %+7.2e +- %7.2e" ) % name % param % err );
      }
    } else {
      calib_message( MSG::WARNING, rad, " fit FAILED" );
    }
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception whilst printing fit parameters '", excpt.what(), "'" );
  }

  // Do we need to draw the results ?
  try {
    if ( m_createPDFsummary ) {

      // make the pads for the plots
      canvas()->cd();
      auto upPad = std::make_unique<TPad>( "U", "U", 0., 0.25, 1., 1. );
      auto dnPad = std::make_unique<TPad>( "L", "L", 0., 0., 1., 0.25 );
      // upPad->SetTopMargin(0.05);
      upPad->SetBottomMargin( 0.1 );
      dnPad->SetTopMargin( 0.05 );
      dnPad->SetBottomMargin( 0.22 );
      upPad->Draw();
      dnPad->Draw();

      // Draw the histogram
      canvas()->cd();
      upPad->cd();
      hist->Draw();

      // Draw vertical line at x=0
      canvas()->Update();
      auto line = std::make_unique<TLine>( 0, upPad->GetUymin(), 0, upPad->GetUymax() );
      line->SetLineStyle( 3 );
      line->Draw();

      // Add fitted functions to PDFs
      if ( fitOK ) {

        // Draw full fit
        bestFitF->SetLineColor( kYellow + 3 );
        bestFitF->Draw( "SAME" );

        // Add scale factor
        const auto old_scale = m_scaleFs[irich];
        const auto new_scale = nScaleFromShift( bestFitF->GetParameter( 1 ), bestFitF->GetParError( 1 ), rad );
        TText      text;
        text.SetNDC();
        text.SetTextSize( 0.025 );
        const auto runS = "Run " + std::to_string( runNumber() );
        const auto sfS  = "SF  " + std::to_string( old_scale.value ) + " -> " + std::to_string( new_scale.value );
        text.DrawText( 0.14, 0.840, runS.c_str() );
        text.DrawText( 0.14, 0.814, sfS.c_str() );

        // Draw background only fit
        backFunc->SetLineColor( kRed + 3 );
        backFunc->Draw( "SAME" );

        // Draw signal only fit
        upPad->cd();
        auto overlay = std::make_unique<TPad>( "overlay", "overlay", 0, 0, 1, 1 );
        overlay->SetFillStyle( 4000 );
        overlay->SetFillColor( 0 );
        overlay->SetFrameFillStyle( 4000 );
        overlay->Draw();
        overlay->cd();
        sigFunc->SetMaximum( 1.5 * sigFunc->Eval( fitRes.ckShift ) );
        sigFunc->SetLineColor( kBlue + 2 );
        sigFunc->Draw( "AL" );

        // Draw the pull
        auto pull = makePullPlot( *hist, *bestFitF.get() );
        canvas()->cd();
        dnPad->cd();
        pull->Draw();

        // print canvas to file
        // note needs to be in this scope as otherwise not everything is included
        printCanvas();

      } else {
        // print just the histogram
        printCanvas();
      }

    } // create PDF
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception creating PDF '", excpt.what(), "'" );
  }

  // return final fit parameter for shift
  calib_message( MSG::DEBUG, "CK theta fit complete for ", rad );
  return fitRes;
}

//=============================================================================
// Make a pull plot from a histogram and a given function
//=============================================================================
std::unique_ptr<TH1D> RefIndexCalib::makePullPlot( TH1& hist, TF1& func ) const {
  // make the new plot with the same bins and range
  std::ostringstream id;
  id << hist.GetName() << "Pull";
  auto h = std::make_unique<TH1D>( id.str().c_str(), "", //
                                   hist.GetNbinsX(), hist.GetXaxis()->GetXmin(), hist.GetXaxis()->GetXmax() );
  h->GetXaxis()->SetTitle( "" );
  h->GetYaxis()->SetTitle( "Pull" );
  h->GetXaxis()->SetTitleSize( 0.08 );
  h->GetYaxis()->SetTitleSize( 0.08 );
  h->GetXaxis()->SetLabelSize( 0.06 );
  h->GetYaxis()->SetLabelSize( 0.06 );
  h->GetYaxis()->SetTitleOffset( 0.3 );
  h->SetStats( false );

  // set the bins. Note numbering starts at 1 for bins, not 0 :(
  for ( int i = 1; i <= hist.GetNbinsX(); ++i ) {
    // bin center
    const auto binCen = hist.GetBinCenter( i );
    // only make pull entries for points in function range
    if ( binCen < func.GetXmax() && binCen > func.GetXmin() ) {
      // raw bin content
      const auto rawCont = hist.GetBinContent( i );
      // bin error
      const auto rawErr = fabs( hist.GetBinError( i ) );
      // function value
      const auto funcVal = func.Eval( binCen );
      // pull
      const auto pull = ( rawErr > 0 ? ( rawCont - funcVal ) / rawErr : 0.0 );
      // set the bin content and error in the pull plots
      h->SetBinContent( i, pull );
      h->SetBinError( i, 1.0 ); // Correct value if you neglect error on fitted function
    }
  }

  // return
  return h;
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RefIndexCalib )

//-----------------------------------------------------------------------------
