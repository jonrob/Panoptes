/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// local
#include "RichMirrCombFit.h"
// ROOT
#include <TGraphErrors.h>
#include <TH2D.h>

#include <string>

class MirrCombFitter : public RichMirrCombFit {
public:
  MirrCombFitter( std::string rN = "1", std::string tiltName = "" );
  ~MirrCombFitter();

  void GetFitParams( TH2D* ) override;

  void FitSurface( bool );

  void FitBySlice();

  void FitSlices( TGraphErrors* );

  void Fit2D( TGraphErrors* );

private:
  TH2D* m_histo = nullptr;
};
