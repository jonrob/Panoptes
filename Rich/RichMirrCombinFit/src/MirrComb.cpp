/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MirrComb.h"

using namespace std;

MirrComb::MirrComb( int pri, int sec ) : m_pri( pri ), m_sec( sec ) {}

MirrComb::MirrComb( string combName ) {
  m_pri = stoi( combName.substr( 0, 2 ) );
  m_sec = stoi( combName.substr( 3, 2 ) );
  ;
}

int MirrComb::Pri() { return m_pri; }
int MirrComb::Sec() { return m_sec; }

double MirrComb::Z() { return m_z; }
void   MirrComb::Z( double z ) { m_z = z; }

double MirrComb::Y() { return m_y; }
void   MirrComb::Y( double y ) { m_y = y; }

double MirrComb::ZErr() { return m_zErr; }
void   MirrComb::ZErr( double zErr ) { m_zErr = zErr; }

double MirrComb::YErr() { return m_yErr; }
void   MirrComb::YErr( double yErr ) { m_yErr = yErr; }

double MirrComb::Shift() { return m_sinusoidShift; }
void   MirrComb::Shift( double shift ) { m_sinusoidShift = shift; }

double MirrComb::ShiftErr() { return m_sinusoidShiftErr; }
void   MirrComb::ShiftErr( double shiftErr ) { m_sinusoidShiftErr = shiftErr; }
