/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
int binPhi;
if ( trPhiRec < 0. ) {
  binPhi = (int)( trPhiRec / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2 - 1;
  if ( binPhi < 0 ) binPhi = 0;
} else {
  binPhi = (int)( trPhiRec / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2;
  if ( binPhi > m_nPhiBins - 1 ) binPhi = m_nPhiBins - 1;
}

int binTheta = (int)( std::sqrt( trThetaRec ) / ( std::sqrt( 0.5 * pi ) / (double)m_nThetaBins ) );

if ( 3 <= binTheta && binTheta <= 18 ) {

  binNo = binPhi + ( binTheta - 3 ) * m_nPhiBins;

  // random choice
  if ( uniProbDistr( gen ) < binWeights[binNo] ) { usefulTrack = true; }
}
