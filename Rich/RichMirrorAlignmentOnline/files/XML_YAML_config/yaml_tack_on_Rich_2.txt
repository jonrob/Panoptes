---top
# Alignment conditions for mirror segemnts will rotate the mirrors in their
# local coordinate system, where the centre of the mirror is at z=0, y=0, x=R.
# To change the Y position of CoC rotate around Z.

# mirror master volumes (simulate support panel movement)
R2M1Master0: !alignment
  position: [0.0 * mm, 0.0 * mm, 0.0 * mm]
  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]
R2M1Master1: !alignment
  position: [0.0 * mm, 0.0 * mm, 0.0 * mm]
  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]
R2M2Master0: !alignment
  position: [0.0 * mm, 0.0 * mm, 0.0 * mm]
  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]
R2M2Master1: !alignment
  position: [0.0 * mm, 0.0 * mm, 0.0 * mm]
  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]

# mirror segments
---bottom
