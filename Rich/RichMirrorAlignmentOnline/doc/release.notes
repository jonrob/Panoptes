!-----------------------------------------------------------------------------
! Package     : Rich/RichMirrorAlignmentOnline
! Responsible : Paras Naik, Claire Prouve
! Purpose     : Python steering for online mirror alignment
!-----------------------------------------------------------------------------

We have now moved from svn to git and need to use that instead.

Get into the habit of committing on every change (that compiles of course)

! Improvements since the previous "tagged" release of this code was tagged should be noted here, if any:


! Notes pertaining to already "tagged" releases:

! 2019-08-29 - Paras Naik
  - "Tagged" as v4r4p0 (internally, since only Panoptes gets a tag now).
  - Set testing = True in Configuration.py, since we are now preparing for the upgrade
  - Modified SetupHelper.py and trendHelper.py, such that if self.alignments is an empty list
     then no trend plot will be produced
  - Modified RichMirrorAlignmentOnline to support stopDecisionMode from RICH-53.
     Current choice in Configuration.py (stopDecisionMode = 1) results in unchanged
     behavior of RichMirrorAlign w.r.t. version used at the pit at the end of 2018.
     Will investigate implementing stopDecisionMode = 0 later.
  - Updated RichAnalyzer.py to just use the "fake" Online DB for all years after 2016
  - Changed self.experts and self.emails in SetupHelper.py to Paras Naik only
  - Changed the (commented) locations of the Zeroed XML files in Configuration.py
  - Print minDate and maxDate submitted when initializing trendHelper.Plotter()
  - Moved (end-of-)Run2 configurations to SavedConfigurations/2018_Configurations

! 2018-05-10 - Paras Naik
  - "Tagged" as v4r4 (internally, since only Panoptes gets a tag now).
  - In trendHelper, added the capabilities to make alignment trend plots with
     fill number or date/time on the x-axis. One of these or alignment number
     may be selected for the x-axis via the the new trendXaxis option in
     trendHelper.Plotter
  - The trend plot made will now be produced with fill numbers on the x-axis.
  - self.minDate now set to the beginning of 2018 if startDate in
     trendHelper.Plotter is None. The default self.maxDate is the end of 9999.
  - Fixed a bug where there were empty entries in "_flip" TGraphs; these entries
     had default values at the origin and skewed the fill number and date/time
     versions of the trend plots.
  - Changed color values of points on the trend plot to avoid white points when
     the marker color was 100.
  - Improved trend plot dimensions, font sizes, and label placement.
  - main() function of trendHelper now produces plots in every combination of
    plotType = {default, hollow} and trendXaxis = {Fills, Numbers, Dates}
    (for offline production of these plots).
  - Configuration.py now has trendDateRange set to {'minDate': None, 'maxDate': None}
  - Moved trend plotting from near the end of AlignMonitor.performMonitoring()
     to near the end of SetupHelper.finalize(), so that we can include the
     alignment just performed in the trend plot.
  - Now posting the 'hollow', 'Fills' trend plot file to the RICH ELOG every fill.
     The 'hollow', 'Dates' trend plot file is also created every fill,
      but not sent to the RICH ELOG.
     Both plot files are available through AlignmentView.
     None of the other combinations are automatically created or posted.

! 2018-05-09 - Paras Naik
  - Changed the dates that go onto the trend plots as text, to reflect
     exactly the date range for the alignments that were used.
  - Fixed a typo that would exclude alignments from the RICH1 trend plotting
     if the alignments matched the minDate.

! 2018-05-01 - Paras Naik
  - Added Current and Historical Magnification Factors to
     Rich/RichMirrorAlignmentOnline for proper tracking.
  - Replaced /group/rich/AlignmentFiles/ with
    /group/rich/sw/cmtuser/AlignmentRelease/Rich/RichMirrorAlignmentOnline/files/
    in Configuration.py to point to the new storage location.

! 2018-04-26 - Paras Naik
  - "Tagged" as v4r3p1 (internally, since only Panoptes gets a tag now).
  - Change default maxDate in trendHelper to the end of the year 9999
  - Print alignments in the _UpdateTrend.txt file not in self.alignments
     (e.g. exceptionList alignments that have an _UpdateTrend entry),
     then reject them.
  - Then print and reject all alignments that are in self.alignments
      that are not (uncommented) in the _UpdateTrend.txt file.
    This effectively removes test alignments automatically,
     without having to add them to the exceptionList.
  - Note that, for the moment, the trend plots only plot past alignments.
     The alignment just performed is currently not included in the plots.

! 2018-04-24 - Paras Naik
  - "Tagged" as v4r3p0 (internally, since only Panoptes gets a tag now).
  - Set magnFactorsMode back to 0 in the configuration for first alignment of 2018;
     posted new magnFactors in
     /group/rich/AlignmentFiles/MagnifFactors/Rich1/20180424_164201/
     /group/rich/AlignmentFiles/MagnifFactors/Rich2/20180424_145140/
     and now using them in the Configuration.

! 2018-04-23 - Paras Naik
  - Set magnFactorsMode to 2 in the configuration for first alignment of 2018;
     remember to post new magnFactors and change mode back to 0 after first alignment.

! 2018-04-14 - Paras Naik
  - "Tagged" as v4r3 (internally, since only Panoptes gets a tag now). Intended for Panoptes v8r1.
  - Now only telling the RunChangeHandler where the XML files for non-mirror conditions are if we
    are performing the mirror alignment on the current year's data.
  - Add commented entries into _UpdateTrend.txt and _CKresTrend.txt when in testing mode.
    The lines can be removed entirely later, unless we choose to use these alignments in which case
    the leading "# TEST " should be removed, and the v number corrected.

! 2018-03-23 - Paras Naik
  - "Tagged" as v4r2p1 (internally, since only Panoptes gets a tag now).
  - Updated definition of regularizationMode in Configuration.py.
    Other minor description updates were made as well.
  - Current Configuration (for testing on 2017 data, saved as SavedConfigurations/Configuration_2018_runOn2017.py).
  - Configuration.py now ready for 2018 data taking.
    "dataVariant" is now "Collision18".
    "testing" mode is now False for both RICH1 and RICH2 in the Configuration.
    maxDate in "trendDateRange" now changed to None.
  - Edited RichAnalyzer.py so that we automatically use the correct year based on the dataVariant in Configuration.py.

! 2018-02-10 - Paras Naik
  - "Tagged" as v4r2p0 (internally, since only Panoptes gets a tag now).
  - Added trendDateRange to Configuration.py, and edited AlignMonitor.py,
      so that a start and end date can be provided for the automatic trend plots.
      To use the beginning(end) of the current year as the start(end) date,
        set minDate(maxDate) to None.
  - Added the word TEST to email subjects (when it was missing) when in testing mode.
  - Emails now only sent to MirrAlign experts when in testing mode.
  - If the version produced is a non-testing _maybe,
      send an email reminding the experts to update the line in UpdateTrend.txt
      with the proper vN.

! 2017-12-17 - Paras Naik
  - "Tagged" as v4r2 (internally, since only Panoptes gets a tag now).
  - Instead of running trendHelper.main(), AlignMonitor now directly runs trendHelper.Plotter().
      The output file names for the trend plot is now given in AlignMonitor,
      to be stored in the usual alignment working directory.
  - trendHelper.Plotter now also accepts a set of dates that define the range of the trend plots,
     and also a filename.
      startDate = None  will use a default start date
      endDate = None  will use a default end date
      fileName = ""  will default to creating the trend file in /home/pnaik/2018trendplots/
  - Removed self.flips from trendHelper since it was not being used.
  - Changed name of trendHelper.ReadAlignments() to to trendHelper.GetChanges_DB_Polarity().
  - trendHelper.GetChanges_DB_Polarity() had some bugs, which have now been fixed.
  - We now ensure that only alignments that we are actually plotting get checked for DB versions
     and Polarity.
  - Changed the order of some commands in the last portion of the Iterator.
  - Added some alignments to the exception lists for RICH1 and RICH2 trend plots.
     See JIRA RICH-47 for more info.

! 2017-12-14 - Sam Maddrell-Mander
  - Added trendHelper.py, which can generate trend plots for the mirror alignment.
      This can also be run stand-alone as a python script (main function).
      Much credit to Claire Prouve for providing the base code.
  - Added code to AlignMonitor to launch trendHelper.main() and generate trend plots
  - "testing" mode is now True for both RICH1 and RICH2 in the Configuration.

! 2017-12-07 - Paras Naik
  - Created an alignment update and polarity trend file in /group/rich/AlignmentFiles/Logging/Rich{1,2}_UpdateTrend.txt.
      We automatically add to this every time we perform an alignment.
      We back-propagated information from some of the 2017 alignments into these text files.

! 2017-11-23 - Paras Naik
  - "Tagged" as v4r1p1 (internally, since only Panoptes gets a tag now).
  - Added check to cancel the mirror alignment if there are missing RICH calibrations.
  - Now only write YES to lastAlignSuccessFile if setupHelper.savedir exists
      or if the alignment was canceled internally
  - Added Online version of "Zero XML" files to Rich/RichMirrorAlignmentOnline/files/
  - Added fileFinder.py and LHCbStyle.py to
      Rich/RichMirrorAlignmentOnline/python/PyMirrAlignOnline
      for future use in making trend plots every fill
  - If the magnet polarity changed *and* a sanity check fails, in autoUpdate mode,
     we no longer provide the current alignment version to HLT2;
     instead we provide no version and the mirror aligners must solve the problem
     and try to run the mirror alignment again, unless it would be more appropriate to
     create a new version from the _maybe_ file, or give the existing version.

! 2017-10-17 - Paras Naik
  - "Tagged" as v4r1p0 (internally, since only Panoptes gets a tag now).
  - Added configurable combAndMirrSubsets for Rich1 and Rich2 to Configuration,
      to allow us to change the combAndMirrSubsets used; modified the Iterator accordingly.
      Default combAndMirrSubsets now point to
      /group/rich/sw/cmtuser/AlignmentRelease/Rich/RichMirrorAlignmentOnline/files/Rich1CombAndMirrSubsets.txt
      and
      /group/rich/sw/cmtuser/AlignmentRelease/Rich/RichMirrorAlignmentOnline/files/Rich2CombAndMirrSubsets.txt
      which should point to the version of Rich/RichMirrorAlignmentOnline being used at the pit
  - Instead of always using the same name locally for the mirror combination subsets,
      we now use the basename of alignConf.getProp('combAndMirrSubsets')
  - combAndMirrSubsets, startXMLfile, compareXMLfile, warningFactor, and displayMode added to summary.txt
  - print the launch directory from the iterator (since we had no idea what that is)

! 2017-10-12 - Paras Naik
  - "Tagged" as v4r1 (internally, since only Panoptes gets a tag now).
  - Added configurable minFracPhiBinsPopulated, which sets the minimum fraction of phi bins that need to be well-populated in order to have an acceptable fit.
      Preferably this is 0.8, but depends on the current HLT line decision efficiency for each particular RICH detector.
  - Changed name of checkFitQual() to checkEnoughBinsPopulated() in RichAlignmentHelper.
  - Added sanity check to identify poor photon population in too many azimuthal bins.
    This can be useful to prevent an unlikely, but possible, mirror alignment update
    when running conditions are different from those for which we tuned our HLT lines
    (i.e. 13 TeV proton-proton collisions). This is achieved by using the result
    of checkEnoughBinsPopulated() to disable the automatic update if there is
    poor photon population in the phi-bins of any mirror combination.
  - Added configurable autoUpdateNoV. If autoUpdate (initially) is True, then autoUpdateNoV is read from the Configuration.
      If autoUpdateNoV is then True, then in cases where an update is desired but a sanity check has failed,
      the mirror alignment does not send a version decision through the DB update service.
      In this case, the mirror aligner has to decide on the _maybe_ file produced.
  - Minor changes to tiltObj.py to make it compatible with external code by Claire and Sam to produce trend plots.
      This includes a new getChange function (unrelated to the one in v3r0) that returns the mirror tilt changes.

! 2017-10-06 - Paras Naik
  - "Tagged" as v4r0 (internally, since only Panoptes gets a tag now).
  - Set autoUpdate (mirror alignment automatic decision) to True in the Configuration for both RICH1 and RICH2.
  - Update alerts for Data Manager to be compatible with other tracking system alignments' alerts.
  - Changed messageServiceName to be the same as for the tracker system alignments.
  - Send an email to Mirror Aligners if the STOP command is received by the Iterator.
  - Change a lack of final output directory from a WARNING to an ERROR (but this ERROR is not sent to the Data Manager).
  - Change the WARNING about lack of processed events to an INFO message, but only for the Data Manager alerts.
  - Remove Cherenkov angle resolution plots from AlignMonitor.pdf, since the Data Manager does not need to look at them.
  - Changed nominalResolutionSigma for RICH1 to better reflect what is being observed from RICH1
      with the current set of MDCS corrections.
  - Increased requiredEvents (required number of processed events) slightly in the Configuration for both RICH1 and RICH2.
  - Added 2016_AutomaticDecisionConfig.txt and 2016_PreAutomaticDecisionConfig.txt to SavedConfigurations
  - Now we only send a message and (when needed) AlignMonitor.pdf to the Alignment Monitoring ELOG
      if the initial autoUpdate is True.

! 2017-09-05 - Paras Naik
  - "Tagged" as v3r6 (internally, since only Panoptes gets a tag now).
  - Added a new DIM service on our DB update DIM server that may be used to send alert messages to the Shift Leader and Data Manager.
      These alerts are only sent if the initial autoUpdate mode is True.
  - Added a new variable autoUpdate_initial, to keep track of the initial state of the autoUpdate Configurable.

! 2017-08-17 - Paras Naik
  - "Tagged" as v3r5p0 (internally, since only Panoptes gets a tag now).
  - Change to MirrPauseHandler, to invoke the SmartIF<IUpdateableIF> (histogram) update in stop() instead of handle()/DAQ_PAUSE.

! 2017-07-19 - Paras Naik
  - "Tagged" as v3r5 (internally, since only Panoptes gets a tag now).
  - Default autoUpdate in the configuration has been changed to False
      We expect to run in this mode until at least 14 August 2017
  - Added more links to the final email that is sent on a successful alignment.
  - Add the first fill number to the email that is sent on a successful alignment.
  - If the last alignment was unable to save the hlt02 log,
      then we now append to the hlt02 log instead of overwrite it
  - Create a copy of the existing hlt02 log before it gets overwritten or appended to.
  - Created CK resolution trend file in /group/rich/AlignmentFiles/Logging/Rich{1,2}_CKresTrend.txt.
      We automatically add to this every time we perform an alignment.
      We back-propagated information from all alignments since 2016 into this text file.
      A trend plot is currently accessible via https://lbrich.cern.ch
        Check under "Resolution timelines: Mirror"
  - If this is a test alignment, in the Alignment monitoring ELOG it gets status 'Test'.
      However, because there is no 'Test' status in the Alignment monitoring ELOG, the message gets ignored.
        This is OK for now.

! 2017-07-16 - Paras Naik
  - "Tagged" as v3r4p1 (internally, since only Panoptes gets a tag now).
  - Added the final CK angle resolution and error to summary.txt.
  - The CK angle resolution for each iteration is now printed and picked up by the hlt02 log file.
  - Change to RichAnalyzer, to pick up MDCS corrections if need be from new xmlMDCS configurable.
      If xmlMDCS is blank, the MDCS corrections from the CondDB are used.
  - If testing is true AND an update is desired, we now append '_testing' to the
     output v xml file, and we also make sure that DB update is None instead of True.
  - Also, if testing is true, we no longer overwrite the previous polarity file.

! 2017-07-13 - Paras Naik
  - warningFactor is now given to RichMirrCombinFit by RichAlignmentHelper.py

! 2017-07-09 - Paras Naik
  - "Tagged" as v3r4p0 (internally, since only Panoptes gets a tag now).
  - Updated choice of fixed Magnification Factors for RICH1 and for RICH2

! 2017-07-04 - Paras Naik
  - "Tagged" as v3r4 (internally, since only Panoptes gets a tag now).
  - Changed name of AlignSummary.py to AlignMonitor.py
  - Fixed code to provide monitoring histograms properly if the zeroth iteration is the only iteration.
  - Added sanity check to prevent the alignment from updating automatically
     if the changes in the mirror alignment are abnormally large.
  - Added warningFactor configurable to Configuration.py
      This configurable determines how many times the tolerance is the level of the sanity check
      When the magnet polarity changes, this sanity check is currently made looser by a factor of 2 (2 is hard coded!)
  - Added displayMode configurable to Configuration.py
      This configurable allows an alternate plotting style for the monitoring to be tried.
  - 2D histograms with mirror numbers are now created
      They now appear in AlignMonitor.pdf and AlignSummary.pdf 2D Histos
      They are also sent to the monitoring/Presenter for later use
  - Per iteration deltaTheta plots now available for experts in AlignSummary.pdf
  - Added F and M ROOT options to the hist->fit to the deltaTheta plot in AlignMonitor.py
  - Added monitor function to SetupHelper.py, which now performs the monitoring
      separately to the finalize function, and also returns the result of the sanity check.
  - Added small offset to setTrunc and setDiv to make sure the color in the 2D histograms
      is correct if a value is exactly on the boundary.
  - Now posting an "alignment starting" message to the RICH ELOG,
      which gets overwritten by the result if the alignment completes properly.
  - Junk messages in the hlt02 log file are now removed, and the result is placed
      in an "hlt02reduced" log file.
  - Now sending deltaTheta histograms to the monitoring/Presenter without hist->fit already performed.
  - Now sending a message and (when needed) AlignMonitor.pdf to the Alignment Monitoring ELOG.
  - We now only overwrite the old magnet polarity name storage file if the alignment converges.

! 2017-06-20 - Paras Naik
  - "Tagged" as v3r3 (internally, since only Panoptes gets a tag now).
  - Now sending monitoring plots to the Presenter/Monet!
  - Increased range of contour levels, in the hopes of avoiding white squares in 2D plots.
  - Improved color scheme.
  - Changed color scheme such that every tilt beyond four times the tolerances has the same color.
  - Added facility to write a custom message to the ELOG. Now letting the ELOG know
      when an alignment has been canceled.

! 2017-06-11 - Paras Naik
  - Changed 2D histos "printf" text style back to 4.2f
  - Added self.????trunc to tiltObj.py, to contain truncated instead of rounded
      versions of self.????, where ???? is one of {priY, priZ, secY, secZ}.
    You can set the number of digits after the decimal point for the truncation
      if you want to.
    These self.????trunc values have to be initialized after a setChange(), simply use
      setTrunc(digits), or setTrunc() to use the default number of digits which is 2
    Truncated values that result in 0, now get a small kick positive or negative to
      show up as e.g. 0.00 or -0.00
  - Now using truncated instead of rounded mirror compensations for the 2D histos
  - Added self.????div to tiltObj.py, to contain truncated instead of rounded
      versions of self.???? divided by the tolerances, where ???? is one of
      {priY, priZ, secY, secZ}, and the tolerances are provided in a list.
    You can set the number of digits after the decimal point for the truncation
      if you want to.
    These self.????div values have to be initialized after a setChange(), simply use
      setDiv(digits, tolerances), or setDiv() to use the default number of digits
      which is 0, and default tolerances of [0.1,0.1,0.1,0.1]
    Truncated values that result in 0, now get a small kick positive or negative to
      show up as e.g. 0 or -0
  - Create a second summary plot PDF file called _AlignMonitor.pdf,
      in preparation for creating monitoring plots for the Presenter/Monet.
    This file includes the truncated-instead-of-rounded
      mirror-compensations-divided-by-their-tolerances plot.
  - Fixed issue where allDone may not be defined in the Iterator because a line was not reached
  - Changed nominalResolutionSigma values to observed resolutions
      (albeit with the tighter selection criteria provided by Claire)
  - Added startXMLFile to configuration, if testing is not True AND this is specified
      then we start the mirror alignment from that XML file instead of the DB one.

! 2017-06-09 - Paras Naik
  - Change in Iterator flow:
      When we want to cancel the alignment we used to skip the while loop that waits for commands from the Communicator.
      Now we enter the while loop, but immediately send a 'reset' rather than wait for the Communicator.
  - If the mirror alignment does not get to the point where a directory is created to save the mirror alignment output:
      We store the htl02 log file in /group/rich/AlignmentFiles/Logging/
      We send an email notification to those listed in setupHelper.emails
  - Changed System to System_19 in the elog command line; this allows emails to be sent when a new ELOG entry is posted.
      This is not ideal, because "System_19" could change to something else if a new RICH System is added in the ELOG.
  - Added workDir to the .conf file writer for RichMirrCombinFit, in RichAlignmentHelper.py

! 2017-06-03 - Paras Naik
  - "Tagged" as v3r2 (internally, since only Panoptes gets a tag now).
  - We now make a copy of Rich/RichMirrorAlignmentOnline/python/RichMirrorAlignmentOnline/Configuration.py every time we run.
    Keeping track of SavedConfigurations is thus no longer necessary (presuming past alignment outputs will always be preserved),
     but could still be useful in the rare case an edit is made at the pit for a quick Configuration change.
  - Fixed a bug in RichAnalyzer that was keeping the RunChangeHandler from seeing our conditions.
  - Fixed the conditions structure, necessary to run on 2017 data.
  - Changed shutil.copyfile() to shutil.copy2() in several places, to preserve timestamps.
  - Rich1 and Rich2 thresholds now settled upon for 2017.
  - Event threshold for automatic update set to 70% of maximum expected value (based on test 2016 alignments).
  - Automatically adjust scale for 2D plots, keeping colors the same for the same set of values.

! 2017-05-30 - Paras Naik
  - We now record the number of events properly.
  - The hlt02 log now contains the output of the Iterator.
  - We now make a copy of /group/online/dataflow/options/LHCbA/HLT/OnlineEnvBase.py every time we run.

! 2017-05-28 - Paras Naik
  - “Tagged" as v3r1p0 (internally, since only Panoptes gets a tag now).
  - Improved AlignSummary plots.
  - Introduced 'testing' Configurable.
    If this is true, the alignment is in testing mode and we do not enable our DB update DIM service.

! 2017-05-24 - Paras Naik
  - “Tagged" as v3r1 (internally, since only Panoptes gets a tag now).
  - Prepared RichAnalyzer.py to use the git DB and accept 2017 data (and also 2016/2015 data).
  - Introduced 2D mirror compensation heatmaps.
    Split getChange function into setChange and getGraphs, in tiltObj.py, to facilitate this.
  - Summary file now being copied from our work area to the stored alignments area again.
  - Un-hardcoded the length of tiltNames and call it tiltNamesLength.
  - Added a subprocess that obtains the changes to the hlt02 messages log while the Iterator is running.
    Unfortunately, this log does not contain the output of the Iterator yet...
      Need to find the log where this is stored and change the filename.
  - Added elapsed times for RichMirrAlign and RichMirrCombinFit.
  - Created a dictionary elapsed_runtimes for different kinds of runtimes, now passing that in functions.
  - Now detecting the dataVariant and using it to change Brunel settings in RichAnalyzer if needed.
  - Number of events is now averaged over the iterations, instead of cumulative.
  - Configuration previously attributed to dbXMLFile is now attributed to compareXMLFile.
  - Updated RICH2 tolerances to 2017 draft values
  - Updated Configuration to allow the alignment to expect Collision17 data.
  - Un-hardcoded the maximum number of iterations before the alignment gives up, now set from 'maxIterations' Configurable.
  - Introduced 'autoUpdate' Configurable. If we wish to update the DB automatically set this to True.
  - Introduced 'requiredEvents' Configurable.
    If we reconstruct fewer than requiredEvents on the farm (i.e. farm nodes are down),
    then we disable any automatic update of the DB, if the alignment has changed.
  - If we do not automatically update, but the alignment has changed OR there was a change in magnet polarity,
    instead of creating v{N+1}.xml, we make a file called v{N+1}_maybe_{setupHelper.directoryTime}.xml
  - Created getNEventsListList(n_it) function in SetupHelper to allow the Iterator to get nEvents.
  - Changed name of CkResSummary(.py) to AlignSummary(.py).

! Tagged Releases:

! 2017-05-19 - Paras Naik
  - “Tagged" as v3r0 (internally, since only Panoptes gets a tag now).
    In principle, ready for 2017 data taking.
    In practice, the "hacks" of 2017-05-08 (see below) need to be resolved.
    Also, the mechanism is now in place to update the online DB automatically.
  - Moved Update DIM server creation from START to CONFIGURE in the Iterator.
  - DIM server now updates with version of XML that should be in DB (even if it is the existing version), waits 20 seconds, then gets removed.
  - Now printing the savedir in the ELOG entry
  - Now forcing print statements to appear in the log file of the computer running the Iterator, by flushing the stdout buffer at key points.
  - Now uses default compareXMLFile for comparisons if the dbXMLFile string is blank in our Configuration
  - Now records and outputs both LHCbA configuration time [from run(whichRich) to START, during LHCbA configuration] and alignment time [from START to next STATE.READY]
  - Moving to weighted regularization for "minuit" alignment method, as default
  - Added the ability to send e-mail from the online mirror alignment to targeted recipients
    ELOG and email author name is now "MirrAlign Monitor"
  - Added the ability to get fill and magnet polarity info from the Run DB, for our Mirrors HLT line data
    We now skip the mirror alignment if any of the data is of mixed polarity or magnet OFF
    We now force a mirror alignment update if the magnet polarity has changed
  - Corrected a problem where the last possible iteration would not run
    Changed max number of iterations from 9 to 8, un-hardcoded this value
  - Updated RICH1 tolerances to 2017 draft values

! 2017-05-08 - Paras Naik
  - Updated RichAnalyzer with a hack that temporarily uses a version of the condDB that recognizes the HPD QEs, and one that ignores the RunStamp. Must change before startup!
  - In SetupHelper we now say that the alignments that we send to the ELOG are for testing. Must change before startup!
  - Updated RichAlignmentHelper to give a value to nominalResolutionSigma
  - Updated RichAnalyzer to never use LumiSeq and NotPhysicsSeq if they are ever turned on elsewhere
  - Updated RichAnalyzer to remove new RichRecQC Monitors that we do not need.
  - Removed CKThetaQuartzRefractCorrections from the mirror alignment, now using defaults
  - CP: Added tiltObj.py, which makes the CkResSummary functional

! 2017-04-14 - Paras Naik
  - Now writing the number of events into both the ELOG and the summary.txt file

! 2017-04-09 - Paras Naik
  - We should use 'x == y' when comparing values and 'x is y' when comparing identities, fixed
  - 'if x == True:' should just be 'if x:', fixed

! 2017-04-08 - Paras Naik
  - Moved startup of DIM service to when the START command is received.
  - Removed all string.format(**locals()), since its use is not recommended.

! 2017-04-06 - Paras Naik
  - Added info about whether the DB updated automatically to the ELOG and the summaryFile
  - Updated RichAnalyzer.py to use the Brunel().RichSequences needed to keep using pre-RichFuture reconstruction
  - Added UpdateHelper.py, which can signal a DB update via DIM service, but does not yet fully operate. Added skeleton for DB update to Iterator.py

! 2017-04-04 - Paras Naik
  - Now writing mirror alignment summary and plots to the LHCb ELOG

! 2017-03-22 - Paras Naik
  - Switching from PhotonMonitoring to PhotonMonitoringAlignOnline (recently added to RichRecQC) for CkResSummary histos

! 2017-03-16 - Paras Naik (on behalf of Claire Prouve)
 - “Tagged" as v2r3 (internally, since only Panoptes gets a tag now).
 - CP: Now importing TGraph, TMultiGraph, TLine from ROOT for plots
       Added "lastXMLFile” which represents the most final iteration XML
       Added “compareXML” which represents the current DB; changed functions accordingly
       We now make plots of primary and secondary mirror tilts w.r.t. compareXML
       stopTolerance and stopToleranceSec removed and replaced with quad stop tolerances (see RichMirrAlign v19r1)
       Also added regularizationMode, stopToleranceMode, nominalResolutionSigma, stopSigmaFraction (see RichMirrAlign v19r1)
       Changed DataType from ‘2015' to ‘2016’ (PN: we really should un-hard-code this)
       Added brunel.Detectors list (PN: why?)
       Added turnOffL0Decode function because (PN: why?)
       Added getDBXML function, which (PN believes) converts “Online” XML to “Offline XML” by adding the right headers
       Coeffcalibtilt replaces/replaced coeffCalibTilt in the configurables, but only for RICH1 (PN thinks this is strange, but currently leaves it)
 - PN: Changed CkResSummary to use RiCKResLongTightMirror instead of RiCKResLongTight (requires newest RichRecQC being delivered from Rec)
       Edited RichAlignmentHelper.py so that it waits for all the tilt processes to finish, not just one at a time (PN thought this must have been a typo)
       For all files that are read with open(), print an ERROR message if the file does not exist
       Print timestamps when each command is sent from the FSM to the Iterator
       Write elapsed time of the the mirror alignment (for a particular RICH) into summary.txt
       Now we copy the files from the workDir to the MirrorAlignments directory in a way that preserves the original timestamps of the files

! 2016-06-07 - Paras Naik (on behalf of Claire Prouve)
 - Tagged as v2r2 (internally, since only Panoptes gets a tag now).
 - CP: added INFO, Warning, and ERROR to printouts for more clarity
 - CP: added python/PyMirrAlignOnline/CkResSummary.py which produces
       additional, summary plots for the alignment

! 2016-04-12 - Paras Naik
 - Tagged as v2r1. Last	version	with an	SVN tag. We must now move to git!
 - Updated to be compatible with the following core code update(s):
      Rich/RichMirrAlign v18r6
 - CP: Changed it so that we search for 2015 Conditions instead of 2016 Conditions.
     We will need to change it back at some point,
      or create a more dynamic system (with the help of Online).
 - PN: Added the ability to use separate stopTolerances for primary and secondary mirrors,
     new in Rich/RichMirrAlign v18r6
   If stopToleranceSec = 0, then all mirrors use stopTolerance to determine the threshold for alignment
     in this case, the printouts show the updated compensations in mrad, as used to be the case
   Otherwise the threshold for primary mirrors is stopTolerance
         and the threshold for secondary mirrors is stopToleranceSec
     in this new case, the printouts now show the **updated compensations divided by the tolerance**
 - PN: Added PhotonMonitoring monitor to RichAnalyzer,
   to create the type of histograms used by the RICH calibration,
   to produce monitoring plots

! 2016-03-29 - Paras Naik
 - Tagged as v2r0.
 - Updated significantly to be compatible with revamped core code:
      Rich/RichMirrCombinFit v16r0p1
      Rich/RichMirrAlign v18r5
 - Changed variable magnifMode to magnFactorsMode
 - Changed variable coeffCalibrTilt to coeffCalibTilt
 - Changed property name magnifCoeffMode to magnFactorsMode
 - Changed combinFitVariant to combinFitMethod
 - Changed mirrCombinSubset to combAndMirrSubsets
 - Changed _mirrCombinSubset to _combAndMirrSubsets
 - Changed outputResultsFile to mirrCombinFitResults
 - Changed inputHistoFile to inputHistos
 - Added args += 'thisVariant           = ' + str(self.thisNameStr) + '\n'
         args += 'fixSinusoidShift      = ' + str(self.fixSinusoidShift) + '\n'
   to the fitConfigFile in RichAlignmentHelper.py (only fixSinusoidShift added to Configuration.py)
 - Changed "Fm" to "Mm" in thisTuning in Configuration.py
 - Changed "Fv" to "Fm" in thisTuning in Configuration.py
 - RichMirrorAlignment* and RichMirrorAlignmentSettings* have been removed
   from the python/PyMirrAlignOnline/ directory
 - The RichMirrorAlignment has been replaced by the Iterator.
   However RichMirrorAlignment.py has been archived as
   doc/OldRichMirrorAlignment.txt for future reference.
 - The RichMirrorAlignmentSettings have been replaced by the
   Configuration/Configurables. However RichMirrorAlignmentSettings.py has been
   archived as doc/OldRichMirrorAlignmentSettings.txt for future reference.

! 2016-02-28 - Paras Naik
 - Tagged as v1r1.
 - CP: Changed from HltFilterRegEx to Hlt1FilterCode, as the former was
       proprietary and the latter is now supported in the newest version
       of Rec/Brunel.
 - CP: We no longer run setupHelper.finalize(conv, n_it) when the command
       configure* is given to the alignment from state NOT_READY
 - PN: Added SavedConfigurations directory to help keep track of configurations
       and allow them to be reused.

! 2016-02-05 - Paras Naik (committing work by Claire Prouve)
 - Tagged as v1r0. This is now production code and must be maintained as such.
 - Iterator.py is still the Iterator, has been updated, and is operational.
   Some functionality has been transfered to new files which have been added:
   RichAlignmentHelper.py, HistoHelper.py, SetupHelper.py, XMLFileHelper.py
 - RichAnalyzer.py is still the Analyzer, has been updated, and is operational.
 - src/MirrPauseHandler.cpp has been updated. This file makes the Analyzers,
   write the histograms, and then collects and merges them.
 - python/RichMirrorAlignmentOnline has been added to allow settings changes
   via python Configurables.

! 2015-07-07 - Paras Naik (Code from Claire Prouve)
 - Tagged as v0r3
 - Reboot of the Online Alignment code, started by Claire. This code is not
   based off of v0r2 but some unknown intermediary version.
 - RichMirrorAlignment_AtClaireTime.py and
   RichMirrorAlignmentSettings_AtClaireTime.py are the base driver scripts
   that Claire built this alignment off of.
 - RichMirrorAlignment.py and RichMirrorAlignmentSettings.py remain in sync with
   v0r2. The changes should have been applied but were not. The improvements
   should be later integrated into the Online alignment (and eventually Offline
   and Online should be re-synced, as there are improvements Claire made as well)
 - Instead of RichMirrorAlignment.py being the Iterator, now Iterator.py is
   the Iterator.
 - Further changes to Communicator.py and RichAnalyzer.py to get the alignment
   to run Online.
 - Claire has agreed to help maintain this package. Since this version is now
   iterating and capable of producing alignments, Claire or Paras will keep
   the release notes updated.

! 2015-06-15 - Paras Naik
 - Tagged as v0r2
 - This is not a fully functioning online alignment
 - The Analyzer is consistent with Claire's latest Analyzer
 - The Iterator (RichMirrorAlignment.py) is identical to the file by the same
   name that was in Rich/RichMirrorAlignmentGanga/job at 2015-06-15 17:44 CEST
 - The two communicate with each other via the use of RichInfo and PatchInfo
 - The online alignment test runs without ERROR, but I am not sure if it
   actually crunches any data
 - MAJOR NOTE: The name of the settings file to be used is HARDCODED into to
   RichMirrorAlignment.py
 - Good luck

! 2015-05-23 - Chris Jones
 - Add missing package dependencies.

! 2015-05-13 - Roel Aaij
 - Updates to RichAnalyzer.py to handle database tags correctly.

! 2015-04-14 - Paras Naik
 - Tagged as v0r1

! 2015-03-17 - Paras Naik
 - initial import (basically an empty shell)
