# @package RichMirrorAlignmentOnline
# @author Claire Prouve <Claire.Prouve@cern.ch>
# @date   08/07/2015

__author__  = "Claire Prouve <Claire.Prouve@cern.ch>"

### NOTE: these are not installed yet, but they exist and can be provided to MirrCombinFit:
# ("sinusoidShift"       , po::value<double>(& m_sinusoidShift         )->default_value( 0.0 ), "shift parameter in fitting formula; if fixSinusoidShift !=0, it is fixed at this value; othrewise this is just initial value"    )
# ("zeroGlobalFitMean"   , po::value<int   >(& m_zeroGlobalFitMean     )->default_value( 0   ), "default =0 : \"mean\" in global fit is not fixed; if !=0 it is fixed at zero"                                                    )
# ("useGlobalFitMean"    , po::value<int   >(& m_useGlobalFitMean      )->default_value( 0   ), "default =0 : don't; otherwise, call GetGlobalFitMean and use that mean as initial (or fixed) value of m_sinusoidShift"           )
# ("combAndMirrSubsets"  , po::value<string>(& m_combAndMirrSubsetsFile)                      , "File with chosen mirrors and combinations subsets"                                                                               )
# ("backgroundOrder"     , po::value<int   >(& m_backgroundOrder       )->default_value( 2   ), "order for the polynomial background"                                                                                             )
# ("plotOutputLevel"     , po::value<int   >(& m_plotOutputLevel       )->default_value( 2   ), "what plots should be saved (0) plot nothing (1) plot ...(2) only plot fits with chi2 worse than 3.0 (3) plot everything"         )
# ("warningFactor"       , po::value<double>(& m_warningFactor         )->default_value( 20  ), "alerts the alignment shifter if any of the mirrors have shifted more than warningFactor*stopTolerance"                           )
### NOTE: these are not installed yet, but they exist and can be provided to RichMirrAlign:
# ("combAndMirrSubsets"  , po::value<string>(&combAndMirrSubsetsFile  )->default_value( ""         ), "file with subsets of mirror combinations and mirrors"                                                        )
# ("usePremisaligned"    , po::bool_switch  (&usePremisaligned        )->default_value( false, "no"), "if true, then pre-misaligned xml files will be used (MC case), default=false"                                )

import os
from Gaudi.Configuration  import *
import GaudiKernel.ProcessJobOptions
from Configurables import (LHCbConfigurableUser)

class Rich1MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = [ ]
    __slots__ = {
       #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart"             : 0
       ,"MinItStart"             : 0
       #### Do not change these!!! Just DONT!
       ,"Rich"                   : 1
       ,"HistoDir"               : ""
       ### Better not change these, only for very very VERY good reasons
       ,"WorkDir"                : "/group/online/AligWork/Rich1/"
       ,"SaveDir"                : "/group/online/AligWork/MirrorAlignments/"
       ,"ItNrFile"               : "/group/online/dataflow/options/LHCbA/Rich1_Alignment_Reference_File.txt"
       ### May be changed if you know what you are doing, explanations below
       ,"dbXMLFile"              : "/group/online/alignment/Rich1/MirrorAlign/v2.xml"
       ,"coeffCalibTilt"         : 0.7
       ,"phiBinFactor"           : 3
       ,"minAverageBinPop"       : 6
       ,"deltaThetaWindow"       : 8.0
       ,"combinFitMethod"        : 5
       ,"magnFactorsMode"        : 0
       ,"magnifDir"              : "/group/rich/AlignmentFiles/MagnifFactors/Rich1/"
       ,"solutionMethod"         : 0
       ,"regularizationMode"     : 1
       ,"stopToleranceMode"      : 1
       ,"nominalResolutionSigma" : 0
       ,"stopSigmaFraction"      : 0
       ,"stopTolerancePriY"      : 0.1
       ,"stopTolerancePriZ"      : 0.1
       ,"stopToleranceSecY"      : 0.1
       ,"stopToleranceSecZ"      : 0.1
       ,"EvtMax"                 : -1
       ,"dataVariant"            : "Collision16"
       ,"thisCase"               : "online"
       ,"tiltNames"              : ["", "pri_negYzerZ", "pri_posYzerZ", "pri_zerYnegZ", "pri_zerYposZ", "sec_negYzerZ", "sec_posYzerZ", "sec_zerYnegZ", "sec_zerYposZ"]
       ,"fixSinusoidShift"       : 1
       ,"nameStr"                : ""
        }

    _propertyDocDct = {
        "MajItStart"             : """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """
       ,"MinItStart"             : """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """
       ,"Rich"                   : """ Rich1 or Rich2. """
       ,"HistoDir"               : """ Directory where the savesets are being written to. """
       ,"WorkDir"                : """ Directory in which all the output will be written. """
       ,"SaveDir"                : """ Directory in which all output files of the alignment are saved after it has finished """
       ,"ItNrFile"               : """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """
       ,"dbXMLFile"              : """ XML file with mirror tilts that is currently in the DB, for comparison of mirror tilts before and after alignment """
       ,"coeffCalibTilt"         : """ Mirror-tilts applied for the calculation of the magnification coefficients. """
       ,"phiBinFactor"           : """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit."""
       ,"minAverageBinPop"       : """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """
       ,"deltaThetaWindow"       : """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """
       ,"combinFitMethod"        : """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: same as 3 but with unifyWidths """
       ,"magnFactorsMode"        : """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """
       ,"magnifDir"              : """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """
       ,"solutionMethod"         : """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """
       ,"regularizationMode"     : """ regularization mode: default 0: regularization term contains average magnification coefficients as weight; !=0: all weights in the regularization term are 1."""
       ,"stopToleranceMode"      : """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below"""
       ,"nominalResolutionSigma" : """ RICH-dependent nominal Cherenkov angle resolution"""
       ,"stopSigmaFraction"      : """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution"""
       ,"stopTolerancePriY"      : """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """
       ,"stopTolerancePriZ"      : """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """
       ,"stopToleranceSecY"      : """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """
       ,"stopToleranceSecZ"      : """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """
       ,"EvtMax"                 : """ maximal number of events processed by brunel PER NODE (hlt fram has ~1500 nodes) """
       ,"dataVariant"            : """ Right now this is used only for the naming convention. """
       ,"thisCase"               : """ Right now this is used only for the naming convention. """
       ,"tiltNames"              : """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """
       ,"fixSinusoidShift"       : """ 1 : fix at sinusoidShift; 0 : it is not fixed """
       ,"nameStr"                : """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """
        }

    def setNameStr(self):
        thisTuning     = "Mp" + str(self.getProp('minAverageBinPop') )
        thisTuning     += "Wi" + str(self.getProp('deltaThetaWindow') )
        thisTuning     += "Fm" + str(self.getProp('combinFitMethod') )
        thisTuning     += "Mm" + str(self.getProp('magnFactorsMode') )
        thisTuning     += "Sm" + str(self.getProp('solutionMethod') )
        thisNameStr = thisTuning + "_" + str(self.getProp('thisCase') ) + "_" + str(self.getProp('dataVariant') )
        self.setProp("nameStr", thisNameStr)

    ### Dont mess with this filename!!!
    def setHistoDir(self):
        import time
        year = time.strftime("%Y")
        histodir = "/hist/Savesets/" + str(year) + "/LHCbA/AligWrk_Rich" + str(self.getProp("Rich")) + "/"
        self.setProp("HistoDir", histodir)


    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            if self.getProp("MinItStart") < 9:
                if not self.getProp("MajItStart") == 0:
                    self.setProp("MajItStart", 0)
                    print "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
            else:
                majstartit = ( self.getProp("MinItStart") - ( self.getProp("MinItStart") % self.getProp("MajItStart") ) ) / self.getProp("MajItStart")
                if not self.getProp("MajItStart") == majstartit:
                    self.setProp('MajItStart', majstartit)
                    print "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
        if(self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == ""):
            self.setProp("magnifDir", '/group/rich/AlignmentFiles/MagnifFactors/Rich1/')
            print "Warning: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
        if self.getProp("magnFactorsMode") == 0:
            self.setProp("MinItStart", self.getProp("MajItStart") * 9)


    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp('WorkDir', os.path.join(self.getProp('WorkDir'), ''))
        self.setProp('SaveDir', os.path.join(self.getProp('SaveDir'), ''))
        self.setProp('magnifDir', os.path.join(self.getProp('magnifDir'), ''))
        print "Info: Rich1MirrAlignOnConf applied"


class Rich2MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = [ ]

    __slots__ = {
       #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart"             : 0
       ,"MinItStart"             : 0
       #### Do not ever change these!!! Just DONT!
       ,"Rich"                   : 2
       ,"HistoDir"               : ""
       ### Better not change these, only for very very VERY good reasons
       ,"WorkDir"                : "/group/online/AligWork/Rich2/"
       ,"SaveDir"                : "/group/online/AligWork/MirrorAlignments/"
       ,"ItNrFile"               : "/group/online/dataflow/options/LHCbA/Rich2_Alignment_Reference_File.txt"
       ### May be changed if you know what you are doing
       ,"dbXMLFile"              : "/group/online/alignment/Rich2/MirrorAlign/v3.xml"
       ,"coeffCalibTilt"         : 0.3
       ,"phiBinFactor"           : 3
       ,"minAverageBinPop"       : 6
       ,"deltaThetaWindow"       : 4.0
       ,"combinFitMethod"        : 5
       ,"magnFactorsMode"        : 0
       ,"magnifDir"              : "/group/rich/AlignmentFiles/MagnifFactors/Rich2/"
       ,"solutionMethod"         : 0
       ,"regularizationMode"     : 1
       ,"stopToleranceMode"      : 1
       ,"nominalResolutionSigma" : 0
       ,"stopSigmaFraction"      : 0
       ,"stopTolerancePriY"      : 0.1
       ,"stopTolerancePriZ"      : 0.1
       ,"stopToleranceSecY"      : 0.1
       ,"stopToleranceSecZ"      : 0.1
       ,"EvtMax"                 : -1
       ,"dataVariant"            : "Collision16"
       ,"thisCase"               : "online"
       ,"tiltNames"              : ["", "pri_negYzerZ", "pri_posYzerZ", "pri_zerYnegZ", "pri_zerYposZ", "sec_negYzerZ", "sec_posYzerZ", "sec_zerYnegZ", "sec_zerYposZ"]
       ,"fixSinusoidShift"       : 1
       ,"nameStr"                : "name"
        }

    _propertyDocDct = {
        "MajItStart"             : """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """
       ,"MinItStart"             : """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """
       ,"Rich"                   : """ Rich1 or Rich2. """
       ,"HistoDir"               : """ Directory where the savesets are being written to. """
       ,"WorkDir"                : """ Directory in which all the output will be written. """
       ,"SaveDir"                : """ Directory in which all output files of the alignment are saved after it has finished """
       ,"ItNrFile"               : """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """
       ,"dbXMLFile"              : """ XML file with mirror tilts that is currently in the DB, for comparison of mirror tilts before and after alignment """
       ,"coeffCalibTilt"         : """ Mirror-tilts applied for the calculation of the magnification coefficients. """
       ,"phiBinFactor"           : """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit."""
       ,"minAverageBinPop"       : """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """
       ,"deltaThetaWindow"       : """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """
       ,"combinFitMethod"        : """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: Same as 3 but with unifyWidths """
       ,"magnFactorsMode"        : """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """
       ,"magnifDir"              : """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """
       ,"solutionMethod"         : """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """
       ,"regularizationMode"     : """ regularization mode: default 0: regularization term contains average magnification coefficients as weight; !=0: all weights in the regularization term are 1."""
       ,"stopToleranceMode"      : """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below"""
       ,"nominalResolutionSigma" : """ RICH-dependent nominal Cherenkov angle resolution"""
       ,"stopSigmaFraction"      : """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution"""
       ,"stopTolerancePriY"      : """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """
       ,"stopTolerancePriZ"      : """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """
       ,"stopToleranceSecY"      : """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """
       ,"stopToleranceSecZ"      : """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """
       ,"EvtMax"          	 : """ maximal number of events processed by brunel PER NODE (hlt fram has ~1500 nodes) """
       ,"dataVariant"            : """ Right now this is used only for the naming convention. """
       ,"thisCase"               : """ Right now this is used only for the naming convention. """
       ,"tiltNames"              : """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """
       ,"fixSinusoidShift"       : """ 1 : fix at sinusoidShift; 0 : it is not fixed """
       ,"nameStr"                : """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """
        }

    ### Dont mess with this filename!!!
    def setHistoDir(self):
        import time
        year = time.strftime("%Y")
        histodir = "/hist/Savesets/" + str(year) + "/LHCbA/AligWrk_Rich" + str(self.getProp("Rich")) + "/"
        self.setProp("HistoDir", histodir)

    def setNameStr(self):
        thisTuning     = "Mp"+str(self.getProp('minAverageBinPop'))
        thisTuning     += "Wi"+str(self.getProp('deltaThetaWindow'))
        thisTuning     += "Fm"+str(self.getProp('combinFitMethod'))
        thisTuning     += "Mm"+str(self.getProp('magnFactorsMode'))
        thisTuning     += "Sm"+str(self.getProp('solutionMethod'))
        thisNameStr = thisTuning + "_" + str(self.getProp('thisCase') ) + "_" +str(self.getProp('dataVariant') )
        self.setProp("nameStr", thisNameStr)

    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            if self.getProp("MinItStart") < 9:
                if not self.getProp("MajItStart") == 0:
                    self.setProp("MajItStart", 0)
                    print "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
            else:
                majstartit = ( self.getProp("MinItStart") - ( self.getProp("MinItStart") % self.getProp("MajItStart") ) ) / self.getProp("MajItStart")
                if not self.getProp("MajItStart") == majstartit:
                    self.setProp('MajItStart', majstartit)
                    print "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
        if(self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == ""):
            self.setProp("magnifDir", '/group/rich/AlignmentFiles/MagnifFactors/Rich2/')
            print "Warning: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
        if self.getProp("magnFactorsMode") == 0:
            self.setProp("MinItStart", self.getProp("MajItStart") * 9)


    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp('WorkDir', os.path.join(self.getProp('WorkDir'), ''))
        self.setProp('SaveDir', os.path.join(self.getProp('SaveDir'), ''))
        self.setProp('magnifDir', os.path.join(self.getProp('magnifDir'), ''))
        print "Info: Rich2MirrAlignOnConf applied"
