###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.tonic import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import make_odin, default_raw_banks
from PyConf.Algorithms import (Rich__Future__Mon__HitMaps as HitMaps,
                               Rich__Future__Mon__LumiMonitors as LumiMonitors,
                               Rich__Future__Mon__DataTuple as DataTuple,
                               HltRoutingBitsFilter, OdinTypesFilter)

from RecoConf.rich_reconstruction import (default_rich_reco_options,
                                          make_rich_pixels)

from Moore.config import Reconstruction

from MooreOnlineConf.utils import update_and_reset


@configurable
def daq_commissioning(offlineMode=False, enableTuple=False, runNumber=0):
    """ RICH commissioning
    """

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Decoding and pixels
    rich_pixels = make_rich_pixels(reco_opts)

    # The list of algorithms to run
    to_run = []

    # Online histogram handling
    if not offlineMode:
        # Should be first in the list if required
        to_run += [update_and_reset()]

    rb_phys_filter = HltRoutingBitsFilter(
        name="RBFilterPhys",
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 14, 0, 0),  # Physics events
        PassOnError=False)

    rb_smog_filter = HltRoutingBitsFilter(
        name="RBFilterSMOG",
        RawBanks=default_raw_banks('HltRoutingBits'),
        RequireMask=(1 << 15, 0, 0),  # SMOG events
        PassOnError=False)

    # Hit Maps
    hMaps = HitMaps(
        name="HitMaps",
        VerbosePlots=offlineMode,
        OfflinePlots=offlineMode,
        SpatialPlots=offlineMode,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"])
    to_run += [hMaps]

    hMapsPhys = HitMaps(
        name="HitMapsPhys",
        allow_duplicate_instances_with_distinct_names=True,
        VerbosePlots=offlineMode,
        OfflinePlots=offlineMode,
        SpatialPlots=offlineMode,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"])
    to_run += [
        CompositeNode(
            "Phys", [rb_phys_filter, hMapsPhys],
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True)
    ]

    hMapsSMOG = HitMaps(
        name="HitMapsSMOG",
        allow_duplicate_instances_with_distinct_names=True,
        VerbosePlots=offlineMode,
        OfflinePlots=offlineMode,
        SpatialPlots=offlineMode,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"])
    to_run += [
        CompositeNode(
            "SMOG", [rb_smog_filter, hMapsSMOG],
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True)
    ]

    # Lumi Monitors
    hLumi = LumiMonitors(
        name="LumiMonitors",
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"])
    to_run += [hLumi]

    # Ntuple maker.
    if offlineMode and enableTuple:
        rtuple = DataTuple(
            name="DataTuple",
            ODINLocation=make_odin(),
            DecodedDataLocation=rich_pixels["RichDecodedData"],
            RunNumber=runNumber)
        to_run += [rtuple]

    return Reconstruction('rich_comissioning', to_run)
