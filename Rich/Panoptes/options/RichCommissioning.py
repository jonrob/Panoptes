###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from Panoptes.commissioning import daq_commissioning
import glob, os

partition = os.getenv('partition')
runNumber = os.getenv('runNumber')

if partition and runNumber:

    print("Partition", partition, "Run", runNumber)
    options.histo_file = f"/group/rich/commissioning/decoding/rootFiles/RichCommissioning_{runNumber}.root"
    lrunNumber = '%010d' % int(runNumber)
    if partition == 'local':
        path = '/localdisk/rich_tmp_data/runs/'
    elif partition == 'daqarea':
        path = '/daqarea51/rich/'
    else:
        path = '/hlt2/objects/%s/%s/' % (partition, lrunNumber)

    files = sorted(glob.glob(path + "*.mdf"), key=os.path.getmtime)

else:

    # Check what is available
    searchPaths = [
        '/usera/jonesc/NFS/data/RunIII/Tel40Pit/', '/hlt2/objects/RICH/'
    ]
    files = []
    for path in searchPaths:
        files += sorted(
            glob.glob(path + "Run_*.mdf") + glob.glob(path + "*/Run_*.mdf"))
    # file files sorted by m time, newest first
    files.sort(key=os.path.getmtime)
    files.reverse()

    #files = [
    #'/hlt2/objects/RICH/0000231749/Run_0000231749_20220529-135809-095_R1EB09.mdf'
    #'/usera/jonesc/NFS/data/RunIII/Tel40Pit/Run_0000234492_20220620-095917-724_R2EB05.mdf'
    #]

options.evt_max = 100000

options.n_threads = 1

options.print_freq = 50000

#options.event_store = 'EvtStoreSvc'

#options.dddb_tag = 'upgrade/dddb-20210617'
options.geometry_version = "run3/trunk"
options.conditions_version = "master"

options.conddb_tag = 'master'
options.simulation = False

#print("\n".join(files))
options.input_files = files

options.input_type = "MDF"

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
pit_db_loc = '/group/online/hlt/conditions.run3/lhcb-conditions-database'
if os.path.exists(pit_db_loc):
    dd4hep.ConditionsLocation = 'file:' + pit_db_loc

with daq_commissioning.bind(
        offlineMode=True, enableTuple=False, runNumber=runNumber):
    run_reconstruction(options, daq_commissioning)
