#!/bin/bash
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
export CMTCONFIG=x86_64_v3-centos7-gcc10-opt;
#
cd /group/rich/commissioning/decoding/Panoptes;
. setup.${CMTCONFIG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh $*;
#
setup_options_path MONITORING;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` $PANOPTESROOT/options/RichCommissioning-Online.py --application=OnlineEvents;
