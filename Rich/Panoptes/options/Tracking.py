###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Apply loose tracking cuts, for 2022 comissioning
# Not to be used once things stabilise

from PyConf.Algorithms import (
    PrForwardTrackingVelo, VPRetinaFullClusterDecoder,
    VeloRetinaClusterTrackingSIMD, PrMatchNN, PrHybridSeeding)

from RecoConf.hlt1_tracking import (make_VeloClusterTrackingSIMD,
                                    make_velo_full_clusters)

from GaudiKernel.SystemOfUnits import GeV

# FIXME: change to something like https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/1854
#PrForwardTrackingVelo.global_bind(
#    MinQuality=0.0,
#    DeltaQuality=0.0,
#    MinTotalHits=9,
#    MaxChi2PerDoF=50.,
#    MaxChi2XProjection=60.,
#    MaxChi2PerDoFFinal=28.,
#    MaxChi2Stereo=16.,
#    MaxChi2StereoAdd=16.,
#)

make_VeloClusterTrackingSIMD.global_bind(
    algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(
    make_full_cluster=VPRetinaFullClusterDecoder)

# Minimum momentum required
min_p = 10 * GeV
PrForwardTrackingVelo.global_bind(MinP=min_p)
PrMatchNN.global_bind(MinP=min_p)
PrHybridSeeding.global_bind(MinP=min_p)
