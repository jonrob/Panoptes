###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os, multiprocessing
from Moore import options, run_reconstruction
from Panoptes.calibration import standalone_rich_ref_index_calib
from PyConf.Algorithms import Rich__Future__Rec__Calib__RefIndexCalib as RefIndexCalib

from ROOT import gROOT
from Gaudi.Configuration import VERBOSE, DEBUG, INFO
from GaudiKernel.SystemOfUnits import GeV

options.output_level = INFO

# Silence ROOT
gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")

isQMTTest = 'QMTTEST_NAME' in os.environ

myName = (os.environ["QMTTEST_NAME"]
          if isQMTTest else "RichRefIndexCalib_noUT")

options.histo_file = myName + ".root"

# Override previously set defaults
# ROOT file reader currently not thread safe :(
options.n_threads = (1 if options.input_type == 'ROOT' or isQMTTest else
                     multiprocessing.cpu_count())
#options.n_threads = 1
options.evt_max = (5000 if isQMTTest else -1)

# Is UT active in this task ?
noUT = ("noUT" in myName)

# PDF Writer options
ckFitForm = ["AsymNormal:FreeNPol"]

# Time between periodic calibrations
calibTime = (100000 if isQMTTest else 20)
#calibTime = 100000

# Fit retry period
retryPeriod = (20 if isQMTTest else 1)

# Auditors (only with 1 thread)
#if not isQMTTest:
#    options.auditors += ["MemoryAuditor"]

# Enable additional monitoring algorithms ?
do_moni = isQMTTest

# Create run by run histos in calibration algorithm
run_by_run_histos = False

# start scale factors at 1 ?
set_scale_to_one = False

# Output level for calibration alg
out_level = (INFO if isQMTTest else VERBOSE)

# polynominal degree in background fits
nPolFull = (3, 3)

# Use running average ?
RunningAv = not isQMTTest

if noUT:
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT as track_maker
else:
    from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

with track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=True),\
     RefIndexCalib.bind( OutputLevel=out_level,\
                         CreatePDFSummary=not isQMTTest,\
                         OKToPublish=not isQMTTest,\
                         RichFitTypes=(ckFitForm, ckFitForm),\
                         UseRunningAverage=RunningAv,\
                         AllowCalibAtStop=not isQMTTest,\
                         nPolFull=nPolFull,\
                         MinTimeBetweenCalibs=calibTime,\
                         FailedFitRetryPeriod=retryPeriod,\
                         RunByRunHists=run_by_run_histos),\
     standalone_rich_ref_index_calib.bind(noUT=noUT,\
                                          do_data_monitoring=do_moni,\
                                          set_scale_factors_to_one=set_scale_to_one) :
    run_reconstruction(options, standalone_rich_ref_index_calib)
