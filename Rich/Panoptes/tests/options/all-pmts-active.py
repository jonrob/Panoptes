###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence vexrsion 3 (GPL Version 3), copied verbatim in the file "COPYING".  #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from Configurables import LHCbApp, DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    tag = "jonrob/all-pmts-active"
    options.conddb_tag = tag
    LHCbApp().CondDBtag = tag  # Work around PyConf bug
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'
else:
    options.conddb_tag = "upgrade/sim-20210630-vc-md100"
