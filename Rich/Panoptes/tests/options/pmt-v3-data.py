###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import DDDBConf, LHCbApp

options.input_files = [
    "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Run3/BM-20220111-DDDB/30000000/XDST/Brunel_"
    + str(n) + ".xdst" for n in range(100, 150)
]
#print("Input data",options.input_files)
options.input_type = 'ROOT'

options.dddb_tag = "upgrade/dddb-20220111"

options.input_raw_format = 4.3

options.simulation = True

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
