###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from Moore import options
from Configurables import LHCbApp

# This script is dd4hep only

jonPath = "/usera/jonesc/NFS/data/RunIII/Hlt2/LHCb/RefIndexCalib/"
eosPath = "mdf:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/data/RunIII/Hlt2/LHCb/RefIndexCalib/"
usePath = (jonPath if os.path.exists(jonPath) else eosPath) + "2023/"
options.input_files = [
    usePath + "data-{i:04d}.mdf".format(i=i) for i in range(10, 107)
]

options.input_type = "MDF"

LHCbApp().DataType = "Upgrade"

options.simulation = False

options.evt_max = 1000

options.geometry_version = "run3/trunk"
options.conditions_version = "online"
#options.conditions_version = "AlignmentV10_2023_05_09_LHCP"
#options.conditions_version = "alignment2022"
