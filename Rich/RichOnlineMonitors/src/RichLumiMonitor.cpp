/*****************************************************************************\

* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichDAQDefinitions.h"

// ROOT
#include "TString.h"

// Rich DAQ
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// event
#include "Event/ODIN.h"

// STD
#include <array>
#include <cmath>
#include <memory>
#include <mutex>
#include <vector>

namespace Rich::Future::Mon {

  // Use the functional framework
  using namespace Gaudi::Functional;

  class LumiMonitors final : public Consumer<void( const LHCb::ODIN&, const DAQ::DecodedData& ),
                                             LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase>> {

  public:
    /// Standard constructor
    LumiMonitors( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                     KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default}} ) {}

    /// Initialize
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {} );
    }

  private:
    // histograms

    /// Pixel based hit map
    DetectorArray<AIDA::IHistogram2D*> h_pixelMapBeamCrossing  = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMap2BeamCrossing = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMapBeamGas       = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMap2BeamGas      = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMapEmptyEmpty    = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMap2EmptyEmpty   = {{}};
    /// nHits
    DetectorArray<AIDA::IHistogram1D*>             h_nHitsLumiBeamCrossing      = {{}}; // jj
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_nHitsLumiBeamCrossing_side = {{}}; // jj
    DetectorArray<AIDA::IHistogram1D*>             h_nHitsLumiBeamGas           = {{}}; // jj
    DetectorArray<AIDA::IHistogram1D*>             h_nHitsLumiEmptyEmpty        = {{}}; // jj

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {

      bool ok = true;

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        const auto  xRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeX[rich] + 20;
        const auto  xBins  = ( 2 * xRange ) + 1;
        const auto  yRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeY[rich] + 20;
        const auto  yBins  = ( 2 * yRange ) + 1;
        const float xMax   = xRange + 0.5;
        const float yMax   = yRange + 0.5;
        ok &= saveAndCheck( h_pixelMapBeamCrossing[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMapBeamCrossing", rich ), //
                                         "Global Pixel Map Beam Crossing",                  //
                                         -xMax, xMax, xBins,                                //
                                         -yMax, yMax, yBins,                                //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_pixelMapBeamGas[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMapBeamGas", rich ), //
                                         "Global Pixel Map Beam Gas",                  //
                                         -xMax, xMax, xBins,                           //
                                         -yMax, yMax, yBins,                           //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_pixelMapEmptyEmpty[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMapEmptyEmpty", rich ), //
                                         "Global Pixel Map Empty Empty",                  //
                                         -xMax, xMax, xBins,                              //
                                         -yMax, yMax, yBins,                              //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_nHitsLumiBeamCrossing[rich],                                   //  jjj
                            richHisto1D( Rich::HistogramID( "nHitsLumiBeamCrossing", rich ), //
                                         "Number of hits from lumi trigger (Beam Crossing)", //
                                         0, 35000, 3500,                                     //
                                         "nHitsLumiBeamCrossing" ) );                        // jjjj
        ok &= saveAndCheck( h_nHitsLumiBeamGas[rich],                                        //  jjj
                            richHisto1D( Rich::HistogramID( "nHitsLumiBeamGas", rich ),      //
                                         "Number of hits from lumi trigger (Beam Gas)",      //
                                         0, 35000, 3500,                                     //
                                         "nHitsLumiBeamGas" ) );                             // jjjj
        ok &= saveAndCheck( h_nHitsLumiEmptyEmpty[rich],                                     //  jjj
                            richHisto1D( Rich::HistogramID( "nHitsLumiEmptyEmpty", rich ),   //
                                         "Number of hits from lumi trigger (Empty Empty)",   //
                                         0, 35000, 3500,                                     //
                                         "nHitsLumiEmptyEmpty" ) );                          // jjjj
        ok &= saveAndCheck( h_pixelMap2BeamCrossing[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMap2BeamCrossing", rich ), //
                                         "Global Pixel Map 2 Beam Crossing",                 //
                                         -xMax, xMax, xBins,                                 //
                                         -yMax, yMax, yBins,                                 //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_pixelMap2BeamGas[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMap2BeamGas", rich ), //
                                         "Global Pixel Map 2 Beam Gas",                 //
                                         -xMax, xMax, xBins,                            //
                                         -yMax, yMax, yBins,                            //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_pixelMap2EmptyEmpty[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMap2EmptyEmpty", rich ), //
                                         "Global Pixel Map 2 Empty Empty",                 //
                                         -xMax, xMax, xBins,                               //
                                         -yMax, yMax, yBins,                               //
                                         "Global Pixel X", "Global Pixel Y" ) );

        for ( const auto side : Rich::sides() ) {
          ok &= saveAndCheck(
              h_nHitsLumiBeamCrossing_side[rich][side],
              richHisto1D( Rich::HistogramID( Form( "nHitsLumiBeamCrossing_RICH%d_Side%d", rich, side ), rich, side ),
                           "Number of hits from lumi triggers per side (Beam Crossing)", 0, 35000, 3500,
                           "nHitsLumiBeamCrossing_side" ) );
        }
      }
      return StatusCode{ok};
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN&       odin, //
                     const DAQ::DecodedData& data  //
                     ) const override {

      std::lock_guard lock( m_updateLock );

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        // data for this RICH
        unsigned int nHits = 0;
        const auto&  rD    = data[rich];
        // sides per RICH
        for ( const auto side : Rich::sides() ) {
          const auto& pD = rD[side];
          // data for this side
          unsigned int nHitsSide = 0;
          // PD modules per side
          for ( const auto& mD : pD ) {
            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // loop over hits
                  for ( const auto id : rawIDs ) {

                    ++nHits;
                    ++nHitsSide;

                    const auto iGlobalX  = id.ecGlobalPMTFrameX() - id.panel() * 220;
                    const auto iGlobalX2 = id.ecGlobalPMTFrameX2() - id.panel() * 220;
                    const auto iGlobalY  = id.ecGlobalPMTFrameY();
                    if ( id.isHTypePMT() ) {
                      // For large H type PMTs fill a group of 4 corresponding to the effective smaller pixels
                      for ( int i = 0; i < 2; ++i ) {
                        for ( int j = 0; j < 2; ++j ) {
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            fillHisto( h_pixelMapBeamCrossing[rich], iGlobalX + i, iGlobalY + j );
                            fillHisto( h_pixelMap2BeamCrossing[rich], iGlobalX2 + i, iGlobalY + j );
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            fillHisto( h_pixelMapBeamGas[rich], iGlobalX + i, iGlobalY + j );
                            fillHisto( h_pixelMap2BeamGas[rich], iGlobalX2 + i, iGlobalY + j );
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            fillHisto( h_pixelMapEmptyEmpty[rich], iGlobalX + i, iGlobalY + j );
                            fillHisto( h_pixelMap2EmptyEmpty[rich], iGlobalX2 + i, iGlobalY + j );
                          }
                        }
                      }
                    } else {
                      if ( Rich::Rich1 == rich ) {
                        if ( 0 == side ) {
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            fillHisto( h_pixelMapBeamCrossing[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX + 3 - 89 - 21 ) ) + 89 + 21 );
                            fillHisto( h_pixelMap2BeamCrossing[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX2 + 3 - 89 - 21 ) ) + 89 + 21 );
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            fillHisto( h_pixelMapBeamGas[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX + 3 - 89 - 21 ) ) + 89 + 21 );
                            fillHisto( h_pixelMap2BeamGas[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX2 + 3 - 89 - 21 ) ) + 89 + 21 );
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            fillHisto( h_pixelMapEmptyEmpty[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX + 3 - 89 - 21 ) ) + 89 + 21 );
                            fillHisto( h_pixelMap2EmptyEmpty[rich], -( iGlobalY + 1 ),
                                       ( -1 * ( iGlobalX2 + 3 - 89 - 21 ) ) + 89 + 21 );
                          }

                        } else {
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            fillHisto( h_pixelMapBeamCrossing[rich], iGlobalY + 1, iGlobalX + 3 );
                            fillHisto( h_pixelMap2BeamCrossing[rich], iGlobalY + 1, iGlobalX2 + 3 );
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            fillHisto( h_pixelMapBeamGas[rich], iGlobalY + 1, iGlobalX + 3 );
                            fillHisto( h_pixelMap2BeamGas[rich], iGlobalY + 1, iGlobalX2 + 3 );
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            fillHisto( h_pixelMapEmptyEmpty[rich], iGlobalY + 1, iGlobalX + 3 );
                            fillHisto( h_pixelMap2EmptyEmpty[rich], iGlobalY + 1, iGlobalX2 + 3 );
                          }
                        }
                      } else {
                        if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                          fillHisto( h_pixelMapBeamCrossing[rich], iGlobalX + 1, iGlobalY );
                          fillHisto( h_pixelMap2BeamCrossing[rich], iGlobalX2 + 1, iGlobalY );
                        } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                    odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                          fillHisto( h_pixelMapBeamGas[rich], iGlobalX + 1, iGlobalY );
                          fillHisto( h_pixelMap2BeamGas[rich], iGlobalX2 + 1, iGlobalY );
                        } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                          fillHisto( h_pixelMapEmptyEmpty[rich], iGlobalX + 1, iGlobalY );
                          fillHisto( h_pixelMap2EmptyEmpty[rich], iGlobalX2 + 1, iGlobalY );
                        }
                      }
                    }

                  } // hit loop

                } // PD has hits

              } // PDID is valid

            } // PDs
          }   // modules
          if ( odin.triggerType() ==
                   (short unsigned int)LHCb::ODINImplementation::v7::ODIN::TriggerTypes::LumiTrigger &&
               odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing )
            fillHisto( h_nHitsLumiBeamCrossing_side[rich][side], nHitsSide ); // jj
        }                                                                     // panels

        if ( odin.triggerType() == (short unsigned int)LHCb::ODINImplementation::v7::ODIN::TriggerTypes::LumiTrigger ) {
          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing )
            fillHisto( h_nHitsLumiBeamCrossing[rich], nHits ); // jj
          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
               odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 )
            fillHisto( h_nHitsLumiBeamGas[rich], nHits ); // jj
          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam )
            fillHisto( h_nHitsLumiEmptyEmpty[rich], nHits ); // jj
        }
      } // RICHes
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( LumiMonitors )

} // namespace Rich::Future::Mon
