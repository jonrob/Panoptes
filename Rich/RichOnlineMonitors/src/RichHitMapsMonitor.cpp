/*****************************************************************************\

* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichDAQDefinitions.h"

// ROOT
#include "TString.h"

// Rich DAQ
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// event
#include "Event/ODIN.h"

// STD
#include <array>
#include <cmath>
#include <memory>
#include <mutex>
#include <vector>

namespace Rich::Future::Mon {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class HitMaps HitMaps.h
   *
   *  Produces RICH hit maps.
   *
   *  @author Chris Jones
   *  @date   2021-05-20xs
   */

  class HitMaps final : public Consumer<void( const LHCb::ODIN&,                                        //
                                              const DAQ::DecodedData&,                                  //
                                              const Rich::Utils::RichSmartIDs&,                         //
                                              const DAQ::Tel40CableMapping&,                            //
                                              const DAQ::PDMDBEncodeMapping& ),                         //
                                        LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase,              //
                                                                             Rich::Utils::RichSmartIDs, //
                                                                             DAQ::Tel40CableMapping,    //
                                                                             DAQ::PDMDBEncodeMapping>> {

  public:
    /// Standard constructor
    HitMaps( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                     KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default},
                     // input conditions data
                     KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey},
                     KeyValue{"Tel40CableMapping", DAQ::Tel40CableMapping::DefaultConditionKey + "-" + name},
                     KeyValue{"PDMDBEncodeMapping", DAQ::PDMDBEncodeMapping::DefaultConditionKey + "-" + name}} ) {
      // setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins2DHistos", 500 ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
        DAQ::Tel40CableMapping::addConditionDerivation( this );
        DAQ::PDMDBEncodeMapping::addConditionDerivation( this );
        m_start_scan = 0;
        m_stop_scan  = 0;
        // m_start_scan = 15;
        // m_stop_scan = 24;
        // m_start_scan = atoi(getenv("RICHSCANSTART"));
        // m_stop_scan = atoi(getenv("RICHSCANSTOP"));
      } );
    }

  private:
    // histograms

    using PMTPlotsPerEC    = std::array<AIDA::IHistogram2D*, LHCb::RichSmartID::MaPMT::RTypePMTsPerEC>;
    using PMTPlotsPerPDM   = std::array<PMTPlotsPerEC, LHCb::RichSmartID::MaPMT::ECsPerModule>;
    using PMTPlotsPerMod   = std::array<PMTPlotsPerPDM, LHCb::RichSmartID::MaPMT::ModulesPerColumn>;
    using PMTPlotsPerCol   = std::array<PMTPlotsPerMod, LHCb::RichSmartID::MaPMT::MaxModuleColumnsAnyPanel>;
    using PMTPlotsPerPanel = PanelArray<PMTPlotsPerCol>;
    using PMTPlots         = DetectorArray<PMTPlotsPerPanel>;

    /// Hit map for RICH, local coordinates
    DetectorArray<AIDA::IHistogram2D*> h_hitMaps = {{}};
    /// Pixel based hit map
    DetectorArray<AIDA::IHistogram2D*> h_pixelMap  = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_pixelMap2 = {{}};
    /// Occupancy vs slotID
    DetectorArray<AIDA::IHistogram2D*> h_NHitsVsSlotID = {{}};
    /// Occupancy vs bunchID
    DetectorArray<AIDA::IHistogram2D*> h_NHitsVsBunchID = {{}};
    /// Occupancy vs taeID
    DetectorArray<AIDA::IHistogram2D*> h_NHitsVsTaeID = {{}};
    /// slotID vs tell40 bit
    DetectorArray<AIDA::IHistogram2D*> h_SlotIDVsBitID = {{}};
    /// bunchID vs tell40 link (i.e. 100 bits)
    DetectorArray<AIDA::IHistogram2D*> h_BunchIDVsBitID = {{}};
    /// taeID vs tell40 link (i.e. 100 bits)
    DetectorArray<AIDA::IHistogram2D*> h_TaeIDVsBitID = {{}};
    /// Individual maps for each PMT
    std::unique_ptr<PMTPlots> h_pixelMapsCol;
    /// Individual hits for each PMT
    std::unique_ptr<PMTPlots> h_pixelCol;
    /// nHits
    DetectorArray<AIDA::IHistogram1D*>             h_nHits      = {{}}; // jj
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_nHits_side = {{}}; // jj

    const int m_maxTaeIndex = 10;

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;
    // Gaudi::Property<int>               m_start_scan{this, "scanStart", 0};
    // Gaudi::Property<int>               m_stop_scan{this, "scanStop", 0};
    unsigned int m_start_scan{0};
    unsigned int m_stop_scan{0};

  private:
    // properties

    /// Fill individual PMT plots
    Gaudi::Property<bool> m_fillVerbosePMTPlots{this, "VerbosePlots", false, "Create verbose PMT specific hit maps"};
    /// Fill spatial coordiate plots
    Gaudi::Property<bool> m_fillSpacePlots{this, "SpatialPlots", false, "Create spatial hit maps"};
    /// Fill individual PMT plots
    Gaudi::Property<bool> m_enableOfflinePlots{this, "OfflinePlots", false, "Enable (large) offline plots"};

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      PD_LOCAL_POSITIONS_X;
      PD_LOCAL_POSITIONS_Y;

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        if ( m_fillSpacePlots ) {
          ok &= saveAndCheck( h_hitMaps[rich],                                          //
                              richHisto2D( Rich::HistogramID( "localHitMap", rich ),    //
                                           "Local PD Panel Space Point Hit Map",        //
                                           xMinPDLoc[rich], xMaxPDLoc[rich], nBins2D(), //
                                           yMinPDLoc[rich], yMaxPDLoc[rich], nBins2D(), //
                                           "Local Panel X", "Local Panel Y" ) );
        }
        const auto  xRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeX[rich] + 20;
        const auto  xBins  = ( 2 * xRange ) + 1;
        const auto  yRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeY[rich] + 20;
        const auto  yBins  = ( 2 * yRange ) + 1;
        const float xMax   = xRange + 0.5;
        const float yMax   = yRange + 0.5;
        ok &= saveAndCheck( h_pixelMap[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMap", rich ), //
                                         "Global Pixel Map 1",                  //
                                         -xMax, xMax, xBins,                    //
                                         -yMax, yMax, yBins,                    //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_nHits[rich],                                       //  jjj
                            richHisto1D( Rich::HistogramID( "nHits", rich ),     //
                                         "Number of hits",                       //
                                         0, 35000, 35000,                        //
                                         "nHits" ) );                            // jjjj
        ok &= saveAndCheck( h_pixelMap2[rich],                                   //
                            richHisto2D( Rich::HistogramID( "PixelMap2", rich ), //
                                         "Global Pixel Map 2",                   //
                                         -xMax, xMax, xBins,                     //
                                         -yMax, yMax, yBins,                     //
                                         "Global Pixel X", "Global Pixel Y" ) );
        ok &= saveAndCheck( h_NHitsVsSlotID[rich],                                   //
                            richHisto2D( Rich::HistogramID( "NHitsVsSlotID", rich ), //
                                         "# of hits Vs slot ID",                     //
                                         -0.5, 31.5, 32,                             //
                                         -0.5, 1.25e5 - 0.5, 1.25e5,                 //
                                         "Slot ID=abs(bxid-1024)%23", "# of hits" ) );
        ok &= saveAndCheck( h_NHitsVsBunchID[rich],                                   //
                            richHisto2D( Rich::HistogramID( "NHitsVsBunchID", rich ), //
                                         "# of hits Vs bunch ID",                     //
                                         -0.5, 3600 - 0.5, 3600,                      // jj
                                         -0.5, 1.25e5 - 0.5, 1.25e3,                  //
                                         "Bunch ID", "# of hits" ) );
        ok &= saveAndCheck( h_NHitsVsTaeID[rich],                                   //
                            richHisto2D( Rich::HistogramID( "NHitsVsTaeID", rich ), //
                                         "# of hits Vs Tae ID",                     //
                                         -0.5, m_maxTaeIndex - 0.5, m_maxTaeIndex,  // jj
                                         -0.5, 1.25e5 - 0.5, 1.25e3,                //
                                         "TAE ID", "# of hits" ) );
        ok &= saveAndCheck( h_TaeIDVsBitID[rich],                                   //
                            richHisto2D( Rich::HistogramID( "TaeIDVsBitID", rich ), //
                                         "Tae ID Vs global bit ID",                 //
                                         -0.5, 2e5 - 0.5, 2e3,                      //
                                         -0.5, m_maxTaeIndex - 0.5, m_maxTaeIndex,  //
                                         "bit ID", "TAE ID" ) );
        if ( m_enableOfflinePlots ) {
          /*ok &= saveAndCheck( h_NHitsVsBunchID[rich],                                   //
                              richHisto2D( Rich::HistogramID( "NHitsVsBunchID", rich ), //
                                           "# of hits Vs bunch ID",                     //
                                           -0.5, 3600 - 0.5, 3600,                      // jj
                                           -0.5, 1.25e5 - 0.5, 1.25e4,                  //
                                           "Bunch ID", "# of hits" ) );*/
          ok &= saveAndCheck( h_SlotIDVsBitID[rich],                                   //
                              richHisto2D( Rich::HistogramID( "SlotIDVsBitID", rich ), //
                                           "Slot ID Vs global bit ID",                 //
                                           -0.5, 2e5 - 0.5, 2e5,                       //
                                           -0.5, 32 - 0.5, 32,                         //
                                           "bit ID", "Slot ID=abs(bxid-1024)%23" ) );
          ok &= saveAndCheck( h_BunchIDVsBitID[rich],                                   //
                              richHisto2D( Rich::HistogramID( "BunchIDVsBitID", rich ), //
                                           "Bunch ID Vs global bit ID",                 //
                                           -0.5, 2e5 - 0.5, 2e3,                        //
                                           -0.5, 3564 - 0.5, 3564,                      //
                                           "bit ID", "Bunch ID" ) );
        }

        if ( m_fillVerbosePMTPlots ) {
          if ( !h_pixelMapsCol.get() ) { h_pixelMapsCol = std::make_unique<PMTPlots>(); }
          if ( !h_pixelCol.get() ) { h_pixelCol = std::make_unique<PMTPlots>(); }
          for ( const auto side : Rich::sides() ) {
            ok &= saveAndCheck(
                h_nHits_side[rich][side],                                                                //  jjj
                richHisto1D( Rich::HistogramID( Form( "nHits_RICH%d_Side%d", rich, side ), rich, side ), //
                             "Number of hits",                                                           //
                             0, 3500, 3500,                                                              //
                             "nHits" ) );                                                                // jjjj
            for ( std::size_t iCol = 0; iCol < LHCb::RichSmartID::MaPMT::ModuleColumnsPerPanel[rich]; ++iCol ) {
              for ( std::size_t iPDM = 0; iPDM < LHCb::RichSmartID::MaPMT::ModulesPerColumn; ++iPDM ) {
                for ( std::size_t iEC = 0; iEC < LHCb::RichSmartID::MaPMT::ECsPerModule; ++iEC ) {
                  for ( std::size_t iPMT = 0; iPMT < LHCb::RichSmartID::MaPMT::RTypePMTsPerEC; ++iPMT ) {
                    if ( rich == Rich::Rich2 && iPDM != 2 && iPDM != 3 && iPMT > 0 ) { continue; }
                    // temporary correction for RICH2 col number in title. remove when using DD4HEP
#ifdef USE_DD4HEP
                    const auto iColCorr = 0u;
#else
                    const auto iColCorr = ( rich == Rich::Rich2 ? 1 : 0 );
#endif
                    const auto pmt = ( 1000 * ( iCol + iColCorr ) ) + ( 100 * iPDM ) + ( 10 * iEC ) + iPMT;
                    ok &= saveAndCheck( ( *h_pixelMapsCol )[rich][side][iCol][iPDM][iEC][iPMT],                      //
                                        richHisto2D( Rich::HistogramID( Form( "PMTHitMap%05zu", pmt ), rich, side ), //
                                                     Form( "COL%02zu_PDM%zu_EC%zu_PMT%zu Hit Map", iCol + iColCorr,
                                                           iPDM, iEC, iPMT ), //
                                                     -0.5, LHCb::RichSmartID::MaPMT::PixelsPerRow - 0.5,
                                                     LHCb::RichSmartID::MaPMT::PixelsPerRow, //
                                                     -0.5, LHCb::RichSmartID::MaPMT::PixelsPerCol - 0.5,
                                                     LHCb::RichSmartID::MaPMT::PixelsPerCol, //
                                                     "Pixel Row", "Pixel Column" ) );
                    ok &= saveAndCheck(
                        ( *h_pixelCol )[rich][side][iCol][iPDM][iEC][iPMT],                                         //
                        richHisto2D( Rich::HistogramID( Form( "PMTHits%05zu", pmt ), rich, side ),                  //
                                     Form( "COL%02zu_PDM%zu_EC%zu_PMT%zu Hits", iCol + iColCorr, iPDM, iEC, iPMT ), //
                                     0, 256, 256,                                                                   //
                                     0, 64, 64,                                                                     //
                                     "StepID", "Anode Index" ) );
                  }
                }
              }
            }
          }
        }
      }

      return StatusCode{ok};
    }

  private:
    /// Get the plot for a specific PMT
    inline auto getPMTPlot( const LHCb::RichSmartID id ) const {
      // extract some parameters
      const auto rich = id.rich();
      const auto side = id.panel();
      const auto iCol = id.panelLocalModuleColumn();
      const auto iPDM = id.columnLocalModuleNum();
      const auto iEC  = id.elementaryCell();
      const auto iPMT = id.pdNumInEC();
      // sanity check before using as array indices
      assert( h_pixelMapsCol.get() );
      const auto& plts = ( *h_pixelMapsCol );
      assert( (std::size_t)rich < plts.size() );
      assert( (std::size_t)side < plts[rich].size() );
      assert( iCol < plts[rich][side].size() );
      assert( iPDM < plts[rich][side][iCol].size() );
      assert( iEC < plts[rich][side][iCol][iPDM].size() );
      assert( iPMT < plts[rich][side][iCol][iPDM][iEC].size() );
      // return plot pointer
      return plts[rich][side][iCol][iPDM][iEC][iPMT];
    }

    inline auto getPMTHitsPlot( const LHCb::RichSmartID id ) const {
      // extract some parameters
      const auto rich = id.rich();
      const auto side = id.panel();
      const auto iCol = id.panelLocalModuleColumn();
      const auto iPDM = id.columnLocalModuleNum();
      const auto iEC  = id.elementaryCell();
      const auto iPMT = id.pdNumInEC();
      // sanity check before using as array indices
      assert( h_pixelCol.get() );
      const auto& plts = ( *h_pixelCol );
      assert( (std::size_t)rich < plts.size() );
      assert( (std::size_t)side < plts[rich].size() );
      assert( iCol < plts[rich][side].size() );
      assert( iPDM < plts[rich][side][iCol].size() );
      assert( iEC < plts[rich][side][iCol][iPDM].size() );
      assert( iPMT < plts[rich][side][iCol][iPDM][iEC].size() );
      // return plot pointer
      return plts[rich][side][iCol][iPDM][iEC][iPMT];
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN&                odin,           //
                     const DAQ::DecodedData&          data,           //
                     const Rich::Utils::RichSmartIDs& smartIDsHelper, //
                     const DAQ::Tel40CableMapping&    tel40Maps,      //
                     const DAQ::PDMDBEncodeMapping&   pdmdbMaps ) const override {

      std::lock_guard lock( m_updateLock );

      //// Get bxID
      const auto bunchID  = odin.bunchId();
      const auto taeIndex = 10 - odin.timeAlignmentEventIndex();
      const int  slotID   = std::abs( bunchID - 1024 ) % 23;
      //// Get value of stepID
      const auto stepID = odin.calibrationStep();

      int thDAC_ID = 0;
      // Get value of thDAC_ID according to scan values
      if ( m_start_scan > m_stop_scan ) {
        thDAC_ID = m_start_scan - stepID;
      } else if ( m_start_scan < m_stop_scan ) {
        thDAC_ID = stepID + m_start_scan;
      }

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        // data for this RICH
        unsigned int nHits = 0;
        const auto&  rD    = data[rich];
        // sides per RICH
        for ( const auto side : Rich::sides() ) {
          const auto& pD = rD[side];
          // data for this side
          unsigned int nHitsSide = 0;
          // PD modules per side
          for ( const auto& mD : pD ) {
            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // loop over hits
                  for ( const auto id : rawIDs ) {

                    ++nHits;
                    ++nHitsSide;

                    const auto& anodeData = pdmdbMaps.anodeData( id );
                    const auto& tel40Data = tel40Maps.tel40Data( id, anodeData.pdmdb, anodeData.frame );

                    if ( taeIndex < m_maxTaeIndex ) {
                      fillHisto( h_TaeIDVsBitID[rich],
                                 1e5 * static_cast<int>( id.panel() ) +
                                     100 * ( 24 * ( tel40Data.sourceID.data() & 0x3F ) + tel40Data.connector.data() ) +
                                     anodeData.bit.data(),
                                 taeIndex );
                    }

                    if ( m_enableOfflinePlots ) {
                      // get the DB anode data for this hit
                      // info() << "PDMDB " << anodeData << endmsg;
                      // get the DB tel40 data
                      // info() << "Tel40 " << tel40Data << endmsg;
                      fillHisto( h_SlotIDVsBitID[rich],
                                 1e5 * static_cast<int>( id.panel() ) +
                                     100 * ( 24 * ( tel40Data.sourceID.data() & 0x3F ) + tel40Data.connector.data() ) +
                                     anodeData.bit.data(),
                                 slotID );
                      fillHisto( h_BunchIDVsBitID[rich],
                                 1e5 * static_cast<int>( id.panel() ) +
                                     100 * ( 24 * ( tel40Data.sourceID.data() & 0x3F ) + tel40Data.connector.data() ) +
                                     anodeData.bit.data(),
                                 bunchID );
                    }

                    if ( m_fillVerbosePMTPlots ) {
                      fillHisto( getPMTHitsPlot( id ), thDAC_ID, id.anodeIndex() );
                      fillHisto( getPMTPlot( id ), id.ecLocalPMTFrameX(), id.ecLocalPMTFrameY() );
                    }

                    const auto iGlobalX  = id.ecGlobalPMTFrameX() - id.panel() * 220;
                    const auto iGlobalX2 = id.ecGlobalPMTFrameX2() - id.panel() * 220;
                    const auto iGlobalY  = id.ecGlobalPMTFrameY();
                    if ( id.isHTypePMT() ) {
                      // For large H type PMTs fill a group of 4 corresponding to the effective smaller pixels
                      for ( int i = 0; i < 2; ++i ) {
                        for ( int j = 0; j < 2; ++j ) {
                          fillHisto( h_pixelMap[rich], iGlobalX + i, iGlobalY + j );
                          fillHisto( h_pixelMap2[rich], iGlobalX2 + i, iGlobalY + j );
                        }
                      }
                    } else {
                      if ( Rich::Rich1 == rich ) {
                        if ( 0 == side ) {
                          fillHisto( h_pixelMap[rich], -( iGlobalY + 1 ),
                                     ( -1 * ( iGlobalX + 3 - 89 - 21 ) ) + 89 + 21 );
                          fillHisto( h_pixelMap2[rich], -( iGlobalY + 1 ),
                                     ( -1 * ( iGlobalX2 + 3 - 89 - 21 ) ) + 89 + 21 );
                        } else {
                          fillHisto( h_pixelMap[rich], iGlobalY + 1, iGlobalX + 3 );
                          fillHisto( h_pixelMap2[rich], iGlobalY + 1, iGlobalX2 + 3 );
                        }
                      } else {
                        fillHisto( h_pixelMap[rich], iGlobalX + 1, iGlobalY );
                        fillHisto( h_pixelMap2[rich], iGlobalX2 + 1, iGlobalY );
                      }
                    }

                    if ( m_fillSpacePlots ) {
                      // get the spatial coordiates
                      const auto gPos = smartIDsHelper.globalPosition( id );
                      // info() << "ID " << id << endmsg;
                      // info() << "gPos " << gPos << endmsg;
                      const auto lPos = smartIDsHelper.globalToPDPanel( gPos );
                      // info() << "lPos " << lPos << endmsg;
                      fillHisto( h_hitMaps[rich], lPos.x(), lPos.y() );
                    }

                  } // hit loop

                } // PD has hits

              } // PDID is valid

            } // PDs
          }   // modules
          if ( m_fillVerbosePMTPlots ) { fillHisto( h_nHits_side[rich][side], nHitsSide ); }
        } // panels
        fillHisto( h_NHitsVsSlotID[rich], slotID, nHits );
        fillHisto( h_NHitsVsBunchID[rich], bunchID, nHits );
        if ( taeIndex < m_maxTaeIndex ) { fillHisto( h_NHitsVsTaeID[rich], taeIndex, nHits ); }
        // if ( m_enableOfflinePlots ) { fillHisto( h_NHitsVsBunchID[rich], bunchID, nHits ); }
        fillHisto( h_nHits[rich], nHits ); // jj
      }                                    // RICHes
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( HitMaps )

} // namespace Rich::Future::Mon
